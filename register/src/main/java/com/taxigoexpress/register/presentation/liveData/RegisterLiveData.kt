package com.taxigoexpress.register.presentation.liveData

import androidx.lifecycle.MutableLiveData
import com.taxigoexpress.core.domain.entities.UserEntity
import com.taxigoexpress.core.presentation.view.BaseLiveData

data class RegisterLiveData(
    val registerObserver: BaseLiveData<UserEntity>,
    val nameErrorObserver: MutableLiveData<Int> = MutableLiveData(),
    val lastNameErrorObserver: MutableLiveData<Int> = MutableLiveData(),
    val passwordErrorObserver: MutableLiveData<Int> = MutableLiveData(),
    val phoneNumberErrorObserver: MutableLiveData<Int> = MutableLiveData(),
    val emailErrorObserver: MutableLiveData<Int> = MutableLiveData()
) {

}
