package com.taxigoexpress.register.presentation.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import androidx.lifecycle.Observer
import com.taxigoexpress.core.domain.entities.UserEntity
import com.taxigoexpress.core.payment.presentation.fragments.PaymentMethodFragment
import com.taxigoexpress.core.presentation.fragments.BaseFragment
import com.taxigoexpress.core.presentation.utils.clickEvent

import com.taxigoexpress.register.R
import kotlinx.android.synthetic.main.fragment_register_ending_layout.*

/**
 * A simple [Fragment] subclass.
 *
 */
class RegisterEndingFragment : BaseFragment() {


    private lateinit var userEntity: UserEntity

    override fun getResourceLayout(): Int = R.layout.fragment_register_ending_layout

    override fun initView(view: View, savedInstanceState: Bundle?) {
        this.loadArguments()
        this.configureButtons()
    }

    private fun loadArguments() {
        this.arguments?.let {
            this.userEntity = it.getSerializable(USER_ENTITY) as UserEntity
            this.configureView()
        }
    }

    private fun configureView() {
        this.tvTitleEndRegister.text = String.format(
            "!Bien %s!\nTu cuenta ya esta creada!",
            this.userEntity.name
        )
    }

    private fun configureButtons() {

        this.btnPickPaymentMethod.clickEvent().observe(this, Observer {
            this.replaceFragment(
                PaymentMethodFragment.newInstance(isRegistring = true),
                R.id.register_container,
                false
            )
        })

        this.tvRegisterSkip.clickEvent().observe(this, Observer {
            this.activity?.finish()
        })
    }

    companion object {
        private const val USER_ENTITY = "user.entity"

        fun newInstance(userEntity: UserEntity) =
            RegisterEndingFragment().apply {
                this.arguments.apply {
                    userEntity to USER_ENTITY
                }
            }
    }


}
