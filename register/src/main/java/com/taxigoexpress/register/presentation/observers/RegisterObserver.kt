package com.taxigoexpress.register.presentation.observers

import android.view.View
import com.taxigoexpress.core.domain.entities.UserEntity
import com.taxigoexpress.core.presentation.observers.DefaultObserverLiveData
import com.taxigoexpress.register.R
import com.taxigoexpress.register.domain.exceptions.RegisterException
import com.taxigoexpress.register.presentation.liveData.RegisterLiveData
import java.io.IOException

class RegisterObserver(private val mRegisterLiveData: RegisterLiveData) :
    DefaultObserverLiveData<UserEntity>(mRegisterLiveData.registerObserver) {

    override fun onStart() {
        this.mRegisterLiveData.registerObserver.loadingObserver.value = View.VISIBLE
        super.onStart()
    }

    override fun onComplete() {
        this.mRegisterLiveData.registerObserver.loadingObserver.value = View.GONE
    }

    override fun onNext(result: UserEntity) {
        this.mRegisterLiveData.registerObserver.customObserver.value = result
    }

    override fun onError(error: Throwable) {
        this.mRegisterLiveData.registerObserver.loadingObserver.value = View.GONE
        when (error) {
            is IOException -> this.mRegisterLiveData.registerObserver.showErrorObserver.value =
                R.string.no_internet_connection
            is RegisterException -> useCase(error)
            else -> this.mRegisterLiveData.registerObserver.showExceptionObserver.value =
                error.localizedMessage
        }
    }

    private fun useCase(error: RegisterException) {
        when (error.validationType) {
            RegisterException.Type.NO_VALID_NAME -> this.mRegisterLiveData.nameErrorObserver.value =
                R.string.no_valid_name
            RegisterException.Type.NO_VALID_LASTNAME -> this.mRegisterLiveData.lastNameErrorObserver.value =
                R.string.no_valid_lastname
            RegisterException.Type.INVALID_PASSWORD -> this.mRegisterLiveData.passwordErrorObserver.value =
                R.string.sign_in_empty_password_error
            RegisterException.Type.NO_VALID_PHONE_NUMBER -> this.mRegisterLiveData.passwordErrorObserver.value =
                R.string.no_valid_phone_number
            RegisterException.Type.NO_VALID_EMAIL -> this.mRegisterLiveData.passwordErrorObserver.value =
                R.string.sign_in_wrong_email_error
        }
    }

}
