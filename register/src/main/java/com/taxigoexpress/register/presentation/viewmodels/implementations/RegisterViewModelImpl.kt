package com.taxigoexpress.register.presentation.viewmodels.implementations

import com.taxigoexpress.core.domain.entities.UserEntity
import com.taxigoexpress.core.presentation.view.BaseLiveData
import com.taxigoexpress.core.presentation.viewmodel.BaseViewModelLiveData
import com.taxigoexpress.register.domain.useCases.RegisterPassengerUseCase
import com.taxigoexpress.register.presentation.liveData.RegisterLiveData
import com.taxigoexpress.register.presentation.observers.RegisterObserver
import com.taxigoexpress.register.presentation.viewmodels.abstractions.RegisterViewModel
import javax.inject.Inject

class RegisterViewModelImpl
@Inject constructor(
    private val registerPassengerUseCase: RegisterPassengerUseCase
) : BaseViewModelLiveData<UserEntity>(), RegisterViewModel<UserEntity> {

    private val mRegisterLiveData by lazy { RegisterLiveData(this.getCustomLiveData()) }

    override fun register(request: UserEntity) {
        this.registerPassengerUseCase.execute(
            RegisterObserver(mRegisterLiveData),
            request
        )
    }

    override fun observeRegister(): RegisterLiveData =
        this.mRegisterLiveData

    override fun observeResult(): BaseLiveData<UserEntity> =
        this.mRegisterLiveData.registerObserver

    override fun onCleared() {
        this.registerPassengerUseCase.dispose()
        super.onCleared()
    }
}