package com.taxigoexpress.register.presentation.fragments

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.facebook.accountkit.Account
import com.facebook.accountkit.AccountKitError
import com.taxigoexpress.core.domain.entities.UserEntity
import com.taxigoexpress.core.getApplicationComponent
import com.taxigoexpress.core.presentation.fragments.BaseFragment
import com.taxigoexpress.core.presentation.fragments.LoaderFragment
import com.taxigoexpress.register.R
import com.taxigoexpress.core.presentation.managers.AccountKitManager
import com.taxigoexpress.core.presentation.utils.Constants
import com.taxigoexpress.core.presentation.utils.clickEvent

import com.taxigoexpress.register.presentation.components.DaggerRegisterComponent
import com.taxigoexpress.register.presentation.components.RegisterComponent
import com.taxigoexpress.register.presentation.modules.RegisterModule
import com.taxigoexpress.register.presentation.viewmodels.abstractions.RegisterViewModel
import kotlinx.android.synthetic.main.fragment_register_layout.*
import javax.inject.Inject

/**
 * A simple [Fragment] subclass.
 *
 */
class RegisterFragment : BaseFragment(),
    AccountKitManager.Callback {

    @Inject
    lateinit var registerViewModel: RegisterViewModel<UserEntity>

    private val loaderDialog by lazy { LoaderFragment.newInstance(getString(R.string.loading)) }

    private val registerComponent: RegisterComponent by lazy {
        DaggerRegisterComponent.builder()
            .applicationComponent(activity?.getApplicationComponent())
            .registerModule(RegisterModule())
            .build()
    }

    override fun getResourceLayout(): Int = R.layout.fragment_register_layout

    override fun initView(view: View, savedInstanceState: Bundle?) {
        this.registerComponent.inject(this)
        this.configureButtons()
        this.observeResults()
    }

    private fun configureButtons() {
        this.btnContinueRegister.clickEvent().observe(this, Observer {
            this.registerViewModel.register(
                UserEntity(
                    null,
                    this.etRegisterName.text.toString(),
                    this.etRegisterLastname.text.toString(),
                    this.etRegisterPhone.text.toString(),
                    this.etRegisterEmail.text.toString(),
                    this.etRegisterPassword.text.toString()
                )
            )
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == AccountKitManager.APP_REQUEST_CODE && resultCode == AppCompatActivity.RESULT_OK) {
            AccountKitManager.getCurrentAccount(this, object : AccountKitManager.Callback {
                override fun onAuthenticationSuccess(account: Account) {
                    this@RegisterFragment.onAuthenticationSuccess(account)
                }

                override fun onAuthenticationFailed(error: AccountKitError) {
                    this@RegisterFragment.onAuthenticationFailed(error)
                }

            })
        }
    }

    private fun observeResults() {

        with(this.registerViewModel.observeRegister()) {

            this.registerObserver.customObserver.observe(this@RegisterFragment, Observer {
                //                AccountKitManager.getCurrentAccount(this@RegisterFragment, this@RegisterFragment)
                this@RegisterFragment.keyStoreManager?.saveDataWithSafeMode(
                    Constants.USER,
                    it.email
                )
                this@RegisterFragment.keyStoreManager?.saveDataWithSafeMode(
                    Constants.PASS_ID,
                    it.id.toString()
                )
                this@RegisterFragment.keyStoreManager?.saveSensitiveData(
                    Constants.PASSWORD,
                    etRegisterPassword.text.toString()
                )
                this@RegisterFragment.replaceFragment(
                    RegisterEndingFragment.newInstance(it),
                    R.id.register_container,
                    false
                )
            })

            this.registerObserver.loadingObserver.observe(this@RegisterFragment, Observer {
                if (it == View.VISIBLE) {
                    this@RegisterFragment.loaderDialog.show(
                        childFragmentManager,
                        RegisterFragment::class.java.name
                    )
                } else {
                    this@RegisterFragment.loaderDialog.dismiss()
                }
            })

            this.nameErrorObserver.observe(this@RegisterFragment, Observer {
                this@RegisterFragment.showAlertWithResource(it ?: 0)
            })

            this.lastNameErrorObserver.observe(this@RegisterFragment, Observer {
                this@RegisterFragment.showAlertWithResource(it ?: 0)
            })

            this.passwordErrorObserver.observe(this@RegisterFragment, Observer {
                this@RegisterFragment.showAlertWithResource(it ?: 0)
            })

            this.emailErrorObserver.observe(this@RegisterFragment, Observer {
                this@RegisterFragment.showAlertWithResource(it ?: 0)
            })

            this.phoneNumberErrorObserver.observe(this@RegisterFragment, Observer {
                this@RegisterFragment.showAlertWithResource(it ?: 0)
            })

            this.registerObserver.showErrorObserver.observe(this@RegisterFragment, Observer {
                this@RegisterFragment.showAlertWithResource(it ?: 0)
            })

            this.registerObserver.showExceptionObserver.observe(this@RegisterFragment, Observer {
                this@RegisterFragment.showAlert(it)
            })
        }
    }

    override fun onAuthenticationSuccess(account: Account) {
        this@RegisterFragment.replaceFragment(
            RegisterEndingFragment(),
            R.id.register_container,
            false
        )
    }

    override fun onAuthenticationFailed(error: AccountKitError) {
        this@RegisterFragment.replaceFragment(
            RegisterEndingFragment(),
            R.id.register_container,
            false
        )
    }


}
