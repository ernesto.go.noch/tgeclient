package com.taxigoexpress.register.presentation.components

import com.taxigoexpress.core.presentation.components.ApplicationComponent
import com.taxigoexpress.core.presentation.scopes.FragmentScope
import com.taxigoexpress.register.presentation.fragments.RegisterFragment
import com.taxigoexpress.register.presentation.modules.RegisterModule
import dagger.Component


@FragmentScope
@Component(
    dependencies = [ApplicationComponent::class],
    modules = [RegisterModule::class]
)
interface RegisterComponent {
    fun inject(registerFragment: RegisterFragment)
}