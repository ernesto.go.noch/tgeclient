package com.taxigoexpress.register.presentation.activities

import android.os.Bundle
import androidx.lifecycle.Observer
import com.taxigoexpress.core.presentation.activities.BaseActivity
import com.taxigoexpress.core.presentation.utils.clickEvent
import com.taxigoexpress.register.R
import com.taxigoexpress.register.presentation.fragments.RegisterFragment
import kotlinx.android.synthetic.main.content_register_toolbar.*

class RegisterActivity : BaseActivity() {

    override fun getLayoutResId(): Int = R.layout.activity_register_layout

    override fun initView(savedInstanceState: Bundle?) {
        this.replaceFragment(
            RegisterFragment(),
            R.id.register_container,
            false
        )

        this.ivRegisterBack.clickEvent().observe(this, Observer {
            if (this.supportFragmentManager.backStackEntryCount >= 1) {
                this.supportFragmentManager.popBackStack()
            }
        })
    }


}
