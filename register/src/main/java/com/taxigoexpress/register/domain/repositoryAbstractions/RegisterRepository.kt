package com.taxigoexpress.register.domain.repositoryAbstractions

import com.taxigoexpress.core.domain.entities.UserEntity
import io.reactivex.Observable

interface RegisterRepository {
    fun registerUser(userEntity: UserEntity): Observable<UserEntity>
    fun registerPassenger(userEntity: UserEntity): Observable<UserEntity>

}
