package com.taxigoexpress.register.data.repositoryImplementations

import com.taxigoexpress.core.domain.entities.UserEntity
import com.taxigoexpress.register.data.datasourceImplementations.RegisterDataSourceImpl
import com.taxigoexpress.register.domain.repositoryAbstractions.RegisterRepository
import io.reactivex.Observable
import javax.inject.Inject

class RegisterRepositoryImpl
@Inject constructor(private val registerDataSourceImpl: RegisterDataSourceImpl) :
    RegisterRepository {

    override fun registerUser(userEntity: UserEntity): Observable<UserEntity> =
        this.registerDataSourceImpl.registerUser(userEntity)

    override fun registerPassenger(userEntity: UserEntity): Observable<UserEntity> =
        this.registerDataSourceImpl.registerPassenger(
            userEntity.id.toString(),
            userEntity
        )


}