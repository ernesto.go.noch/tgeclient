package com.taxigoexpress.register.data.services

import com.taxigoexpress.core.BuildConfig
import com.taxigoexpress.core.domain.entities.UserEntity
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Path

interface RegisterServices {

    @POST(BuildConfig.REGISTER_USER)
    fun register(
        @Body request: UserEntity
    ): Observable<Response<UserEntity>>

    @POST(BuildConfig.REGISTER_PASSENGER)
    fun registerPassenger(
        @Path("id") userId: String,
        @Body request: UserEntity
    ): Observable<Response<UserEntity>>

}