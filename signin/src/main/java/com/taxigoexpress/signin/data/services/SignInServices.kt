package com.taxigoexpress.signin.data.services

import com.taxigoexpress.core.BuildConfig
import com.taxigoexpress.signin.domain.entities.requestEntities.LoginRequest
import com.taxigoexpress.signin.domain.entities.responseEntities.LoginResponse
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface SignInServices {

    @POST(BuildConfig.LOGIN)
    fun signIn(
        @Body loginRequest: LoginRequest
    ):Observable<Response<Unit>>
}