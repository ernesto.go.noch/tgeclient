package com.taxigoexpress.signin.data.repositoryImplementations

import com.taxigoexpress.signin.data.datasourceImplementations.SignInDataSourceImpl
import com.taxigoexpress.signin.domain.entities.requestEntities.LoginRequest
import com.taxigoexpress.signin.domain.entities.requestEntities.SigninRequest
import com.taxigoexpress.signin.domain.entities.responseEntities.LoginResponse
import com.taxigoexpress.signin.domain.repositoryAbstractions.SignInRepository
import io.reactivex.Observable
import javax.inject.Inject

class SigninRepositoryImpl
@Inject constructor(private val signinDatasourceImpl: SignInDataSourceImpl):SignInRepository{

    override fun signin(params: LoginRequest): Observable<Unit> =
        this.signinDatasourceImpl.signin(params)


    override fun validateEmail(request: String): Observable<Unit> =
        this.signinDatasourceImpl.validateEmail(request)

}