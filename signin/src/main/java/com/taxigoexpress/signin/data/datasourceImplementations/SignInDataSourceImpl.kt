package com.taxigoexpress.signin.data.datasourceImplementations

import com.taxigoexpress.signin.data.services.SignInServices
import com.taxigoexpress.signin.domain.datasourceAbstractions.SignInDataSource
import com.taxigoexpress.signin.domain.entities.requestEntities.LoginRequest
import io.reactivex.Observable
import javax.inject.Inject

class SignInDataSourceImpl
@Inject constructor(private val service: SignInServices) : SignInDataSource {

    override fun signin(params: LoginRequest): Observable<Unit> =
        this.service.signIn(params)
            .flatMap {
                if (it.isSuccessful) {
                    Observable.just(Unit)
                } else {
                    Observable.error(Throwable(it.message()))
                }
            }

    override fun validateEmail(request: String) =
        Observable.just(Unit)


}
