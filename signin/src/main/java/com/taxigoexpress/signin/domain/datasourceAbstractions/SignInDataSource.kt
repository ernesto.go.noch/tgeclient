package com.taxigoexpress.signin.domain.datasourceAbstractions

import com.taxigoexpress.signin.domain.entities.requestEntities.LoginRequest
import com.taxigoexpress.signin.domain.entities.requestEntities.SigninRequest
import com.taxigoexpress.signin.domain.entities.responseEntities.LoginResponse
import io.reactivex.Observable

interface SignInDataSource {
    fun validateEmail(request: String): Observable<Unit>
    fun signin(params: LoginRequest): Observable<Unit>

}
