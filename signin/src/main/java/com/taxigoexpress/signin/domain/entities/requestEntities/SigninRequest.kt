package com.taxigoexpress.signin.domain.entities.requestEntities

data class SigninRequest (
    private val _email:String? = null,
    private val _password:String? = null
){
    val email:String
    get() = _email ?: ""

    val password:String
    get() = _password ?: ""
}
