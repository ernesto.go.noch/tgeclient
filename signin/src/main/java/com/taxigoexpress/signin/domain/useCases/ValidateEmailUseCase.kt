package com.taxigoexpress.signin.domain.useCases

import com.taxigoexpress.core.domain.executors.PostExecutionThread
import com.taxigoexpress.core.domain.executors.ThreadExecutor
import com.taxigoexpress.core.domain.useCases.UseCase
import com.taxigoexpress.core.presentation.utils.isValidEmail
import com.taxigoexpress.signin.domain.exceptions.SigninException
import com.taxigoexpress.signin.domain.repositoryAbstractions.SignInRepository
import io.reactivex.Observable
import javax.inject.Inject

class ValidateEmailUseCase
@Inject constructor(
    private val signinRepository: SignInRepository,
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread)
    : UseCase<Unit,String>(threadExecutor, postExecutionThread){

    override fun createObservable(params: String): Observable<Unit> = when {
        !params.isValidEmail() -> Observable.error(SigninException(SigninException.Type.EMAIL_WRONG_FORMAT))
        else -> signinRepository.validateEmail(params)
    }

}