package com.taxigoexpress.signin.domain.exceptions

class SigninException
    @JvmOverloads constructor(
        val validationType: Type, message: String? = "SignIn ${validationType.name}", cause: Throwable? = null
    ) :
            Throwable(message, cause) {

    enum class Type {
        EMAIL_WRONG_FORMAT,
        PASSWORD_EMPTY_ERROR
    }
}