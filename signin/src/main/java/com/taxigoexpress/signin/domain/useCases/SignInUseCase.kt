package com.taxigoexpress.signin.domain.useCases

import com.taxigoexpress.core.domain.executors.PostExecutionThread
import com.taxigoexpress.core.domain.executors.ThreadExecutor
import com.taxigoexpress.core.domain.useCases.UseCase
import com.taxigoexpress.core.presentation.utils.isValidEmail
import com.taxigoexpress.signin.domain.entities.requestEntities.LoginRequest
import com.taxigoexpress.signin.domain.entities.responseEntities.LoginResponse
import com.taxigoexpress.signin.domain.exceptions.SigninException
import com.taxigoexpress.signin.domain.repositoryAbstractions.SignInRepository
import io.reactivex.Observable
import javax.inject.Inject

class SignInUseCase
@Inject constructor(
    private val signinRepository: SignInRepository,
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread
) : UseCase<Unit, LoginRequest>(threadExecutor, postExecutionThread) {

    override fun createObservable(params: LoginRequest): Observable<Unit> = when {
        params._password.isEmpty() -> Observable.error(SigninException(SigninException.Type.PASSWORD_EMPTY_ERROR))
        !params._email.isValidEmail() -> Observable.error(SigninException(SigninException.Type.EMAIL_WRONG_FORMAT))
        else -> this.signinRepository.signin(params)
    }
}