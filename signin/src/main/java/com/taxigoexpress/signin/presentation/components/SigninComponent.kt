package com.taxigoexpress.signin.presentation.components

import com.taxigoexpress.core.presentation.components.ApplicationComponent
import com.taxigoexpress.core.presentation.scopes.FragmentScope
import com.taxigoexpress.signin.presentation.fragments.SetLoginPasswordFragment
import com.taxigoexpress.signin.presentation.fragments.SigninEmailFragment
import com.taxigoexpress.signin.presentation.modules.SigninModule
import dagger.Component

@FragmentScope
@Component(
    dependencies = [ApplicationComponent::class],
    modules = [SigninModule::class]
)
interface SigninComponent {
    fun inject(signinEmailFragment: SigninEmailFragment)
    fun inject(setLoginPasswordFragment: SetLoginPasswordFragment)
}