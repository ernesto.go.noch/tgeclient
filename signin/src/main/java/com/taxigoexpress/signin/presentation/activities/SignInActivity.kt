package com.taxigoexpress.signin.presentation.activities

import android.os.Bundle
import com.ia.mchaveza.kotlin_library.gone
import com.ia.mchaveza.kotlin_library.visible
import com.taxigoexpress.core.presentation.activities.BaseActivity
import com.taxigoexpress.signin.R
import com.taxigoexpress.signin.presentation.fragments.SigninEmailFragment
import kotlinx.android.synthetic.main.activity_sign_in_layout.*

class SignInActivity : BaseActivity() {

    override fun getLayoutResId(): Int = R.layout.activity_sign_in_layout

    override fun initView(savedInstanceState: Bundle?) {
        setSupportActionBar(signinToolbar)
        this.replaceFragment(SigninEmailFragment(), R.id.sign_in_container, false)
    }

    fun hideToolbar() {
        this.signinAppbar.gone()
    }

    fun showToolbar() {
        this.signinAppbar.visible()
    }

}
