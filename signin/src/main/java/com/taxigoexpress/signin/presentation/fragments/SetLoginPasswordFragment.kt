package com.taxigoexpress.signin.presentation.fragments

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.taxigoexpress.core.getApplicationComponent
import com.taxigoexpress.core.presentation.fragments.BaseFragment
import com.taxigoexpress.core.presentation.fragments.LoaderFragment
import com.taxigoexpress.core.presentation.utils.clickEvent
import com.taxigoexpress.register.presentation.activities.RegisterActivity
import com.taxigoexpress.register.presentation.fragments.RegisterFragment
import com.taxigoexpress.signin.R
import com.taxigoexpress.signin.domain.entities.requestEntities.LoginRequest
import com.taxigoexpress.signin.presentation.activities.SignInActivity
import com.taxigoexpress.signin.presentation.components.DaggerSigninComponent
import com.taxigoexpress.signin.presentation.components.SigninComponent
import com.taxigoexpress.signin.presentation.modules.SigninModule
import com.taxigoexpress.signin.presentation.viewmodels.abstractions.SigninViewModel
import kotlinx.android.synthetic.main.fragment_set_login_password_layout.*
import org.jetbrains.anko.intentFor
import javax.inject.Inject

/**
 * A simple [Fragment] subclass.
 *
 */
class SetLoginPasswordFragment : BaseFragment() {

    @Inject
    lateinit var signInViewModel: SigninViewModel<Unit>

    private val loaderDialog by lazy { LoaderFragment.newInstance(getString(com.taxigoexpress.register.R.string.loading)) }

    private val signInComponent: SigninComponent by lazy {
        DaggerSigninComponent.builder()
            .applicationComponent(activity?.getApplicationComponent())
            .signinModule(SigninModule())
            .build()
    }

    private lateinit var strEmail: String

    companion object {
        private const val EMAIL_PARAM = "email.param"

        fun newInstance(strEmail: String): SetLoginPasswordFragment {
            val fragment = SetLoginPasswordFragment()
            val args = Bundle()
            args.putString(EMAIL_PARAM, strEmail)
            fragment.arguments = args
            return fragment
        }
    }

    override fun getResourceLayout(): Int = R.layout.fragment_set_login_password_layout

    override fun initView(view: View, savedInstanceState: Bundle?) {
        this.signInComponent.inject(this)
        this.loadArguments()
        this.observeResults()
        this.configureButtons()
        this.configureToolbar()
    }

    private fun configureToolbar() {
        this.showToolbar()
    }

    private fun loadArguments() {
        val args = this.arguments
        args?.let {
            this.strEmail = args.getString(EMAIL_PARAM) ?: ""
        }
        (this.activity as SignInActivity).showToolbar()
    }

    private fun configureButtons() {
        this.btn_continue_signin.clickEvent().observe(this, Observer {
            this.signInViewModel.signin(
                LoginRequest(
                    this.strEmail,
                    this.et_password.text.toString()
                )
            )
        })
    }

    private fun observeResults() {
        with(this.signInViewModel.observeSignIn()) {
            this.signInObserver.customObserver.observe(this@SetLoginPasswordFragment, Observer {
                this@SetLoginPasswordFragment.activity?.finish()
            })

            this.signInObserver.loadingObserver.observe(this@SetLoginPasswordFragment, Observer {
                if (it == View.VISIBLE) {
                    this@SetLoginPasswordFragment.loaderDialog.show(
                        childFragmentManager,
                        RegisterFragment::class.java.name
                    )
                } else {
                    this@SetLoginPasswordFragment.loaderDialog.dismiss()
                }
            })


            this.signInObserver.showErrorObserver.observe(this@SetLoginPasswordFragment, Observer {
                this@SetLoginPasswordFragment.showAlertWithResource(it ?: 0)
            })

            this.signInObserver.showExceptionObserver.observe(
                this@SetLoginPasswordFragment,
                Observer {
                    this@SetLoginPasswordFragment.showAlert("Error al inicio de sesión")
                })

            this.emailErrorObserver.observe(this@SetLoginPasswordFragment, Observer {
                this@SetLoginPasswordFragment.showAlertWithResource(it ?: 0)
            })

            this.passwordErrorObserver.observe(this@SetLoginPasswordFragment, Observer {
                this@SetLoginPasswordFragment.showAlertWithResource(it ?: 0)
            })
        }
    }


}
