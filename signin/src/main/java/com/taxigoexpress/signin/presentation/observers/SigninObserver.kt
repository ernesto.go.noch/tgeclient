package com.taxigoexpress.signin.presentation.observers

import android.view.View
import com.taxigoexpress.core.presentation.observers.DefaultObserverLiveData
import com.taxigoexpress.signin.R
import com.taxigoexpress.signin.domain.entities.responseEntities.LoginResponse
import com.taxigoexpress.signin.domain.exceptions.SigninException
import com.taxigoexpress.signin.presentation.liveData.SignInLiveData
import java.io.IOException

class SigninObserver(private val mSigninLiveData: SignInLiveData) :
    DefaultObserverLiveData<Unit>(mSigninLiveData.signInObserver) {

    override fun onStart() {
        this.mSigninLiveData.signInObserver.loadingObserver.value = View.VISIBLE
        super.onStart()
    }

    override fun onComplete() {
        this.mSigninLiveData.signInObserver.loadingObserver.value = View.GONE
    }

    override fun onNext(result: Unit) {
        this.mSigninLiveData.signInObserver.customObserver.value = result
    }

    override fun onError(error: Throwable) {
        this.mSigninLiveData.signInObserver.loadingObserver.value = View.GONE
        when (error) {
            is IOException -> this.mSigninLiveData.signInObserver.showErrorObserver.value =
                R.string.no_internet_connection
            is SigninException -> useCase(error)
            else -> this.mSigninLiveData.signInObserver.showExceptionObserver.value = error.localizedMessage
        }
    }

    private fun useCase(error: SigninException) {
        when (error.validationType) {
            SigninException.Type.PASSWORD_EMPTY_ERROR -> this.mSigninLiveData.passwordErrorObserver.value =
                R.string.sign_in_empty_password_error
            SigninException.Type.EMAIL_WRONG_FORMAT-> this.mSigninLiveData.emailErrorObserver.value =
                R.string.sign_in_wrong_email_error
        }
    }

}
