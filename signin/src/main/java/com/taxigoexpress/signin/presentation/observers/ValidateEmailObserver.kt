package com.taxigoexpress.signin.presentation.observers

import android.view.View
import com.taxigoexpress.core.presentation.observers.DefaultObserverLiveData
import com.taxigoexpress.core.presentation.view.BaseLiveData
import com.taxigoexpress.signin.R
import com.taxigoexpress.signin.domain.exceptions.SigninException
import java.io.IOException

class ValidateEmailObserver (private val validateEmailLiveData: BaseLiveData<Unit>) :
    DefaultObserverLiveData<Unit>(validateEmailLiveData) {

    override fun onStart() {
        this.validateEmailLiveData.loadingObserver.value = View.VISIBLE
        super.onStart()
    }

    override fun onComplete() {
        this.validateEmailLiveData.loadingObserver.value = View.GONE
    }

    override fun onNext(result: Unit) {
        this.validateEmailLiveData.customObserver.value = result
    }

    override fun onError(error: Throwable) {
        this.validateEmailLiveData.loadingObserver.value = View.GONE
        when (error) {
            is IOException -> this.validateEmailLiveData.showErrorObserver.value =
                R.string.no_internet_connection
            is SigninException -> useCase(error)
            else -> this.validateEmailLiveData.showExceptionObserver.value = error.localizedMessage
        }
    }

    private fun useCase(error: SigninException) {
        when (error.validationType) {
                SigninException.Type.EMAIL_WRONG_FORMAT-> this.validateEmailLiveData.showErrorObserver.value =
                        R.string.sign_in_wrong_email_error
        }
    }

}
