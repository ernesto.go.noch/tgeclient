package com.taxigoexpress.signin.presentation.viewmodels.abstractions

import com.taxigoexpress.core.presentation.view.BaseLiveData
import com.taxigoexpress.core.presentation.viewmodel.PresenterLiveData
import com.taxigoexpress.signin.domain.entities.requestEntities.LoginRequest
import com.taxigoexpress.signin.domain.entities.requestEntities.SigninRequest
import com.taxigoexpress.signin.presentation.liveData.SignInLiveData

interface SigninViewModel<T> : PresenterLiveData<T> {
    fun validateEmail(request: String)
    fun signin(request: LoginRequest)
    fun observeEmailValidation(): BaseLiveData<Unit>
    fun observeSignIn(): SignInLiveData
}