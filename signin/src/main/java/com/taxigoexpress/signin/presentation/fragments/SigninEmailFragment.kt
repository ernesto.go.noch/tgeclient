package com.taxigoexpress.signin.presentation.fragments


import androidx.lifecycle.Observer
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import com.taxigoexpress.core.getApplicationComponent
import com.taxigoexpress.core.presentation.fragments.BaseFragment
import com.taxigoexpress.core.presentation.fragments.LoaderFragment
import com.taxigoexpress.core.presentation.utils.Constants
import com.taxigoexpress.core.presentation.utils.clickEvent

import com.taxigoexpress.signin.R
import com.taxigoexpress.signin.domain.entities.requestEntities.LoginRequest
import com.taxigoexpress.signin.domain.entities.responseEntities.LoginResponse
import com.taxigoexpress.signin.presentation.activities.SignInActivity
import com.taxigoexpress.signin.presentation.components.DaggerSigninComponent
import com.taxigoexpress.signin.presentation.components.SigninComponent
import com.taxigoexpress.signin.presentation.modules.SigninModule
import com.taxigoexpress.signin.presentation.viewmodels.abstractions.SigninViewModel
import kotlinx.android.synthetic.main.fragment_signin_layout.*
import javax.inject.Inject

/**
 * A simple [Fragment] subclass.
 *
 */
class SigninEmailFragment : BaseFragment() {

    @Inject
    lateinit var signInViewModel: SigninViewModel<Unit>

    private val loaderDialog by lazy { LoaderFragment.newInstance(getString(com.taxigoexpress.register.R.string.loading)) }

    private val signInComponent: SigninComponent by lazy {
        DaggerSigninComponent.builder()
            .applicationComponent(activity?.getApplicationComponent())
            .signinModule(SigninModule())
            .build()
    }

    override fun getResourceLayout(): Int = R.layout.fragment_signin_layout

    override fun initView(view: View, savedInstanceState: Bundle?) {
        this.signInComponent.inject(this)
        this.configureButtons()
        this.observeResults()
        this.configureToolbar()
        this.keyStoreManager?.retrieveDataWithSafeMode(Constants.USER)?.let {
            this.startSession()
        }
    }

    private fun startSession() {
        this.signInViewModel.signin(
            this.keyStoreManager?.retrieveDataWithSafeMode(Constants.USER)?.let { user ->
                this.keyStoreManager?.retrieveSensitiveData(Constants.PASSWORD)?.let { password ->
                     LoginRequest(
                        user,
                        password
                    )
                }
            }!!
        )
    }

    private fun configureToolbar() {
        (this.activity as SignInActivity).hideToolbar()
    }

    private fun configureButtons() {
        this.etEmail.setText(
            this.keyStoreManager?.retrieveDataWithSafeMode(Constants.USER)
        )
        this.btnContinue.clickEvent().observe(this, Observer {
            this.signInViewModel.validateEmail(
                this.etEmail.text.toString()
            )
        })
    }

    private fun observeResults() {

        with(this.signInViewModel.observeEmailValidation()) {
            this.customObserver.observe(this@SigninEmailFragment, Observer {
                this@SigninEmailFragment.replaceFragment(
                    SetLoginPasswordFragment.newInstance(
                        this@SigninEmailFragment.etEmail.text.toString()
                    ), R.id.sign_in_container, true
                )
            })

            this.loadingObserver.observe(this@SigninEmailFragment, Observer {

            })


            this.showErrorObserver.observe(this@SigninEmailFragment, Observer {
                this@SigninEmailFragment.showAlertWithResource(it ?: 0)
            })

            this.showExceptionObserver.observe(this@SigninEmailFragment, Observer {
                this@SigninEmailFragment.showAlert(message = it ?: "")
            })
        }

        with(this.signInViewModel.observeSignIn()) {
            this.signInObserver.customObserver.observe(this@SigninEmailFragment, Observer {
                this@SigninEmailFragment.activity?.finish()
            })

            this.signInObserver.loadingObserver.observe(this@SigninEmailFragment, Observer {
                if (it == View.VISIBLE) {
                    this@SigninEmailFragment.loaderDialog.show(
                        childFragmentManager,
                        SigninEmailFragment::class.java.name
                    )
                } else {
                    this@SigninEmailFragment.loaderDialog.dismiss()
                }
            })


            this.signInObserver.showErrorObserver.observe(this@SigninEmailFragment, Observer {
                this@SigninEmailFragment.showAlertWithResource(it ?: 0)
            })

            this.signInObserver.showExceptionObserver.observe(
                this@SigninEmailFragment,
                Observer {
                    this@SigninEmailFragment.showAlert("Error al inicio de sesión")
                })

            this.emailErrorObserver.observe(this@SigninEmailFragment, Observer {
                this@SigninEmailFragment.showAlertWithResource(it ?: 0)
            })

            this.passwordErrorObserver.observe(this@SigninEmailFragment, Observer {
                this@SigninEmailFragment.showAlertWithResource(it ?: 0)
            })
        }
    }


}
