package com.taxigoexpress.signin.presentation.liveData

import androidx.lifecycle.MutableLiveData
import com.taxigoexpress.core.presentation.view.BaseLiveData
import com.taxigoexpress.signin.domain.entities.responseEntities.LoginResponse

data class SignInLiveData(
    val signInObserver: BaseLiveData<Unit>,
    val emailErrorObserver:MutableLiveData<Int> = MutableLiveData(),
    val passwordErrorObserver: MutableLiveData<Int> = MutableLiveData()
)
