package com.taxigoexpress.signin.presentation.modules

import com.taxigoexpress.core.presentation.scopes.FragmentScope
import com.taxigoexpress.signin.data.repositoryImplementations.SigninRepositoryImpl
import com.taxigoexpress.signin.data.services.SignInServices
import com.taxigoexpress.signin.domain.entities.responseEntities.LoginResponse
import com.taxigoexpress.signin.domain.repositoryAbstractions.SignInRepository
import com.taxigoexpress.signin.presentation.viewmodels.abstractions.SigninViewModel
import com.taxigoexpress.signin.presentation.viewmodels.implementations.SigninViewModelImpl
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Named

@Module
class SigninModule {

    @Provides
    @FragmentScope
    fun providesSignInServices(@Named("main_retrofit") retrofit: Retrofit): SignInServices =
        retrofit.create(SignInServices::class.java)

    @Provides
    @FragmentScope
    fun providesSignInRepository(signInRepositoryImpl: SigninRepositoryImpl): SignInRepository =
        signInRepositoryImpl

    @Provides
    @FragmentScope
    fun provideSignInViewModel(signInViewModelImpl: SigninViewModelImpl): SigninViewModel<Unit> =
        signInViewModelImpl
}