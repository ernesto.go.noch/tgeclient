package com.taxigoexpress.signin.presentation.viewmodels.implementations

import androidx.lifecycle.MutableLiveData
import com.taxigoexpress.core.presentation.view.BaseLiveData
import com.taxigoexpress.core.presentation.viewmodel.BaseViewModelLiveData
import com.taxigoexpress.signin.domain.entities.requestEntities.LoginRequest
import com.taxigoexpress.signin.domain.entities.requestEntities.SigninRequest
import com.taxigoexpress.signin.domain.entities.responseEntities.LoginResponse
import com.taxigoexpress.signin.domain.useCases.SignInUseCase
import com.taxigoexpress.signin.domain.useCases.ValidateEmailUseCase
import com.taxigoexpress.signin.presentation.liveData.SignInLiveData
import com.taxigoexpress.signin.presentation.observers.SigninObserver
import com.taxigoexpress.signin.presentation.observers.ValidateEmailObserver
import com.taxigoexpress.signin.presentation.viewmodels.abstractions.SigninViewModel
import javax.inject.Inject

class SigninViewModelImpl
@Inject constructor(
    private val validateEmailUseCase: ValidateEmailUseCase,
    private val signInUseCase: SignInUseCase
) : BaseViewModelLiveData<Unit>(), SigninViewModel<Unit> {

    private val mEmailValidationLiveData by lazy {
        this.buildLiveData(
            MutableLiveData<Unit>()
        )
    }
    private val mSigninLiveData by lazy {
        SignInLiveData(this.getCustomLiveData()) }

    override fun validateEmail(request: String) {
        this.validateEmailUseCase.execute(
            ValidateEmailObserver(this.mEmailValidationLiveData),
            request
        )
    }

    override fun signin(request: LoginRequest) {
        this.signInUseCase.execute(
            SigninObserver(this.mSigninLiveData),
            request
        )
    }

    override fun observeEmailValidation(): BaseLiveData<Unit> =
        this.mEmailValidationLiveData

    override fun observeSignIn(): SignInLiveData =
        this.mSigninLiveData

    override fun observeResult(): BaseLiveData<Unit> =
        this.mSigninLiveData.signInObserver

    override fun onCleared() {
        this.signInUseCase.dispose()
        this.validateEmailUseCase.dispose()
        super.onCleared()
    }
}