package com.taxigoexpress.client.trips.domain.datasourceAbstractions

import com.taxigoexpress.client.trips.domain.entities.requestEntities.PreviewTripRequest
import com.taxigoexpress.client.trips.domain.entities.requestEntities.TripEntity
import com.taxigoexpress.client.trips.domain.entities.requestEntities.TripStatisticsRequest
import com.taxigoexpress.client.trips.domain.entities.responseEntities.PriceResponse
import com.taxigoexpress.client.trips.domain.entities.responseEntities.TripResponse
import com.taxigoexpress.client.trips.domain.entities.responseEntities.TripStatisticsResponse
import com.taxigoexpress.client.trips.domain.entities.responseEntities.TripsInformationResponse
import io.reactivex.Observable

interface TripsDataSource {
    fun postTrip(tripEntity: TripEntity): Observable<TripEntity>
    fun getTripTypes(): Observable<Unit>
    fun getTripsHistory(): Observable<List<TripEntity>>
    fun getTripsStatistics(request: TripStatisticsRequest): Observable<TripStatisticsResponse>
    fun calculatePrices(
        request: PreviewTripRequest,
        serviceType: PriceResponse.ServiceTypeEnum
    ): Observable<PriceResponse>

    fun getTripInformation(
        origin: String, destination: String
    ): Observable<TripsInformationResponse>

    fun updateTrip(tripId: String): Observable<TripEntity>
}
