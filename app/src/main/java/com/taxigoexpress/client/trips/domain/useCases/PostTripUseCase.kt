package com.taxigoexpress.client.trips.domain.useCases

import com.github.nkzawa.socketio.client.IO
import com.taxigoexpress.client.trips.domain.entities.requestEntities.TripEntity
import com.taxigoexpress.client.trips.domain.entities.responseEntities.PriceResponse
import com.taxigoexpress.client.trips.domain.entities.responseEntities.TripResponse
import com.taxigoexpress.client.trips.domain.entities.responseEntities.TripsInformationResponse
import com.taxigoexpress.client.trips.domain.repositoryAbstractions.TripsRepository
import com.taxigoexpress.core.domain.executors.PostExecutionThread
import com.taxigoexpress.core.domain.executors.ThreadExecutor
import com.taxigoexpress.core.domain.useCases.UseCase
import io.reactivex.Observable
import java.net.Socket
import java.util.*
import javax.inject.Inject

class PostTripUseCase
@Inject constructor(
    private val tripsRepository: TripsRepository,
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread
) : UseCase<TripEntity, Pair<PriceResponse, TripsInformationResponse.LegsResponse>>(
    threadExecutor,
    postExecutionThread
) {

    override fun createObservable(params: Pair<PriceResponse, TripsInformationResponse.LegsResponse>): Observable<TripEntity> =
        this.tripsRepository.postTrip(this.generateRequest(params))

    private fun generateRequest(params: Pair<PriceResponse, TripsInformationResponse.LegsResponse>): TripEntity {
        val tripInformation = params.second
        val price = params.first
        return TripEntity(
            null,
            price._passId,
            tripInformation._startAddress,
            tripInformation._endAddress,
            tripInformation._startLocation?._lat,
            tripInformation._endLocation?._lat,
            tripInformation._startLocation?._lng,
            tripInformation._endLocation?._lng,
            tripInformation._startLocation?._lat,
            tripInformation._startLocation?._lng,
            "",
            "",
            price._price?.toFloat(),
            "",
            "",
            "",
            "",
            "",
            0
        )
    }


}