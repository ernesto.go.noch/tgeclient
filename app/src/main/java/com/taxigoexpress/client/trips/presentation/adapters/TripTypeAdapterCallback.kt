package com.taxigoexpress.client.trips.presentation.adapters

import com.taxigoexpress.client.trips.domain.entities.responseEntities.PriceResponse

interface TripTypeAdapterCallback {
    fun onSelectType(price: PriceResponse)
}
