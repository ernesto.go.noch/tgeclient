package com.taxigoexpress.client.trips.presentation.viewmodel.abstraction

import com.taxigoexpress.core.presentation.view.BaseLiveData
import com.taxigoexpress.core.presentation.viewmodel.PresenterLiveData

interface TripsHistoryViewModel<T> : PresenterLiveData<T> {
    fun getTripsHistory(passId: String)
    fun observeTripsHistory(): BaseLiveData<T>
}