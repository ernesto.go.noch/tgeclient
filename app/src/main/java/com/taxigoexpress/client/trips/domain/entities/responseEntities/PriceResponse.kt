package com.taxigoexpress.client.trips.domain.entities.responseEntities

import java.io.Serializable

data class PriceResponse(

    val _serviceType: Int? = null,
    val _price: String? = null,
    val _passId:Int

):Serializable {

    val serviceType: ServiceTypeEnum
        get() {
            return when (_serviceType) {
                1 -> ServiceTypeEnum.Basic
                2 -> ServiceTypeEnum.Premium
                else -> ServiceTypeEnum.Deluxe
            }
        }

    enum class ServiceTypeEnum(val type: Int) {
        Basic(1),
        Premium(2),
        Deluxe(3)
    }
}
