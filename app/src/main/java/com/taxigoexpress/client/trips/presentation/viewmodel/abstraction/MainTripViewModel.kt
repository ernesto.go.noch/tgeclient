package com.taxigoexpress.client.trips.presentation.viewmodel.abstraction

import com.taxigoexpress.client.trips.domain.entities.responseEntities.TripsInformationResponse
import com.taxigoexpress.core.presentation.view.BaseLiveData
import com.taxigoexpress.core.presentation.viewmodel.PresenterLiveData
import com.taxigoexpress.places.model.PlaceDetails

interface MainTripViewModel<T> : PresenterLiveData<T> {
    fun calculatePrice(request: TripsInformationResponse.LegsResponse)
    fun observablePrice(): BaseLiveData<T>
    fun getTripInformation(params:Pair<PlaceDetails, PlaceDetails>)
    fun observableTripInformation(): BaseLiveData<TripsInformationResponse>
}