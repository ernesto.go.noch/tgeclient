package com.taxigoexpress.client.trips.presentation.fragments

import android.location.Location
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.beust.klaxon.JsonObject
import com.beust.klaxon.Parser
import com.beust.klaxon.array
import com.google.android.gms.maps.model.LatLng
import com.taxigoexpress.client.R
import com.taxigoexpress.client.trips.presentation.utils.Utilities.ADDRESS
import com.taxigoexpress.client.trips.presentation.utils.Utilities.PLACE_ID
import com.taxigoexpress.client.trips.presentation.utils.Utilities.RESULTS
import com.taxigoexpress.client.trips.presentation.utils.Utilities.getGeocodeUrl
import com.taxigoexpress.core.presentation.fragments.BaseDialogFragment
import com.taxigoexpress.core.presentation.fragments.LoaderFragment
import com.taxigoexpress.core.presentation.utils.clickEvent
import com.taxigoexpress.core.presentation.utils.hideKeyboard
import com.taxigoexpress.places.PlaceAPI
import com.taxigoexpress.places.adapter.PlacesAutoCompleteAdapter
import com.taxigoexpress.places.listener.OnPlacesDetailsListener
import com.taxigoexpress.places.model.Place
import com.taxigoexpress.places.model.PlaceDetails
import kotlinx.android.synthetic.main.fragment_search_places_layout.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.onComplete
import org.jetbrains.anko.uiThread
import java.net.URL

/**
 * Efectivo y Tarjeta
 * A simple [Fragment] subclass.
 */
class SearchPlacesFragment : BaseDialogFragment() {

    private var currentLocationFounded: Location? = null
    private var originPlaceId: String? = null
    private var callback: SearchPlacesCallback? = null
    private var destination: Place? = null
    private var origin: Place? = null
    private var currentAddress: String? = null

    private val placesApi by lazy {
        this.activity?.let {
            PlaceAPI.Builder()
                .apiKey(this.getString(R.string.api_key_places))
                .build(it)
        }
    }

    private val loaderDialog by lazy { LoaderFragment.newInstance(getString(R.string.loading)) }

    override fun getStyle(): Int? = com.taxigoexpress.core.R.style.FullScreenDialog

    override fun getLayout(): Int = R.layout.fragment_search_places_layout

    override fun initView(view: View, savedInstanceState: Bundle?) {
        this.configureSearchViews()
        this.configureButtons()
    }

    fun setFoundedLocation(location: Location) {
        this.currentLocationFounded = location
    }


    private fun configureButtons() {
        this.ivSearchPlacesClose.clickEvent().observe(this, Observer {
            this.dismiss()
        })

        this.btnAddHomeDestination.clickEvent().observe(this, Observer {
            AddHomeFragment().show(
                this@SearchPlacesFragment.childFragmentManager,
                AddHomeFragment::class.java.name
            )
        })

        this.btnAddWorkDestination.clickEvent().observe(this, Observer {
            AddWorkFragment().show(
                this@SearchPlacesFragment.childFragmentManager,
                AddWorkFragment::class.java.name
            )
        })

        this.btnHomeDestination.clickEvent().observe(this, Observer {

        })

        this.btnWorkDestination.clickEvent().observe(this, Observer {

        })

        this.tvUseCurrentLocation.clickEvent().observe(this, Observer {
            this.swUseCurrentLocation.isEnabled = !this.swUseCurrentLocation.isEnabled
        })

        this.swUseCurrentLocation.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                this.currentLocationFounded?.let { this.getCurrentDirection(it) }
            }
        }
    }

    private fun getCurrentDirection(currentLocationFounded: Location) {
        this.loaderDialog.show(
            childFragmentManager,
            SearchPlacesFragment::class.java.name
        )
        val url = getGeocodeUrl(
            LatLng(
                currentLocationFounded.latitude,
                currentLocationFounded.longitude
            )
        )
        doAsync {
            val result = URL(url).readText()
            uiThread {
                val parser = Parser()
                val stringBuilder: StringBuilder = StringBuilder(result)
                val json: JsonObject = parser.parse(stringBuilder) as JsonObject
                // get to the correct element in JsonObject
                val results = json.array<JsonObject>(RESULTS)
                currentAddress = results?.get(0)?.get(ADDRESS) as String
                originPlaceId = results[0][PLACE_ID] as String
                origin = Place(
                    originPlaceId!!,
                    currentAddress!!
                )
            }
            onComplete {
                currentAddress?.let { address -> setCurrentLocationAsOrigin(address) }
            }
        }
    }

    private fun setCurrentLocationAsOrigin(currentAddress: String) {
        if (currentAddress.isNotEmpty()) {
            actOrigin.setText(currentAddress)
            this.hideKeyboard()
        }
        this.loaderDialog.dismiss()
    }


    fun setCallback(callback: SearchPlacesCallback) {
        this.callback = callback
    }

    private fun configureSearchViews() {

        this.actDestination.hint = this.getString(R.string.pick_destination)
        this.actOrigin.hint = this.getString(R.string.pick_origin)

        placesApi?.let { api ->
            this.actOrigin.setAdapter(this.context?.let {
                PlacesAutoCompleteAdapter(
                    it,
                    api,
                    false
                )
            })
            this.actDestination.setAdapter(this.context?.let {
                PlacesAutoCompleteAdapter(
                    it,
                    api,
                    false
                )
            })
        }

        this.actOrigin.onItemClickListener =
            AdapterView.OnItemClickListener { parent, _, position, _ ->
                val place = parent.getItemAtPosition(position) as Place
                this.actOrigin.setText(place.description)
                this.hideKeyboard()
                origin = place
                this.destination?.let {
                    this.hideSearchPlaces()
                }
            }

        this.actDestination.onItemClickListener =
            AdapterView.OnItemClickListener { parent, _, position, _ ->
                val place = parent.getItemAtPosition(position) as Place
                this.actDestination.setText(place.description)
                this.hideKeyboard()
                destination = place
                this.origin?.let {
                    this.hideSearchPlaces()

                }
            }
    }

    private fun hideSearchPlaces() {
        this.origin?.let {
            this.destination?.let { destination ->
                this.callback?.onSelectedPlaces(
                    it,
                    destination
                )
            }
        }
        this.dismiss()
    }

    companion object {
        fun newInstance() = SearchPlacesFragment()
    }
}
