package com.taxigoexpress.client.trips.presentation.fragments


import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.taxigoexpress.client.R
import com.taxigoexpress.core.presentation.fragments.BaseDialogFragment
import com.taxigoexpress.core.presentation.utils.clickEvent
import com.taxigoexpress.core.presentation.utils.hideKeyboard
import com.taxigoexpress.places.PlaceAPI
import com.taxigoexpress.places.adapter.PlacesAutoCompleteAdapter
import com.taxigoexpress.places.model.Place
import kotlinx.android.synthetic.main.fragment_add_home_layout.*

/**
 * A simple [Fragment] subclass.
 */
class AddHomeFragment : BaseDialogFragment() {

    private var placeSelected: Place? = null

    override fun getStyle(): Int? = R.style.FullScreenDialog

    override fun getLayout(): Int = R.layout.fragment_add_home_layout

    override fun initView(view: View, savedInstanceState: Bundle?) {
        this.initFinder()
        this.configureButtons()
    }

    private fun initFinder() {
        val placesApi = this.activity?.let {
            PlaceAPI.Builder()
                .apiKey(this.getString(R.string.api_key_places))
                .build(it)
        }

        placesApi?.let { api ->
            this.atcAddHome.setAdapter(this.context?.let {
                PlacesAutoCompleteAdapter(it, api, true)
            })
        }

        this.atcAddHome.onItemClickListener =
            AdapterView.OnItemClickListener { parent, _, position, _ ->
                val place = parent.getItemAtPosition(position) as Place
                this.hideKeyboard()
                this.atcAddHome.setText(place.description)
                placeSelected = place
            }
    }

    private fun configureButtons() {
        this.ivAddHomeClose.clickEvent().observe(this, Observer {
            this.dismiss()
        })
    }
}
