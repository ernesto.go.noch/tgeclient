package com.taxigoexpress.client.trips.presentation.utils

import android.annotation.SuppressLint
import android.view.MotionEvent
import android.view.View
import java.text.SimpleDateFormat
import java.util.*


object TripsUtils {

    @SuppressLint("SimpleDateFormat")
    private val dateFormatter = SimpleDateFormat("yyyy-MM-dd")

    enum class Period {
        DAY,
        WEEK,
        MONTH,
        YEAR
    }

    fun getStartDate(period: Period): String {
        val date = when (period) {
            Period.DAY -> this.getCurrentDate()
            Period.WEEK -> this.getStartWeekDate()
            Period.MONTH -> this.getStartMonthDate()
            else -> this.getStartYearDate()
        }
        return this.formattedDate(date)
    }

    private fun getCurrentDate(): Date =
        Calendar.getInstance().time

    private fun getStartWeekDate(): Date {
        val calendar = Calendar.getInstance()
        calendar.get(Calendar.WEEK_OF_MONTH)
        return calendar.time
    }

    private fun getStartMonthDate(): Date {
        val calendar = Calendar.getInstance()
        calendar.get(Calendar.MONTH)
        return calendar.time
    }

    private fun getStartYearDate(): Date {
        val calendar = Calendar.getInstance()
        calendar.get(Calendar.YEAR)
        return calendar.time
    }

    fun getEndWeekDay(): Date {
        val startDate = getStartWeekDate()
        val calendar = Calendar.getInstance()
        calendar.time = startDate
        calendar.add(Calendar.DAY_OF_WEEK, 7)
        return calendar.time
    }

    fun getEndMonthDay(): Date {
        val startDate = getStartMonthDate()
        val calendar = Calendar.getInstance()
        calendar.time = startDate
        calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE))
        return calendar.time
    }

    fun formattedDate(date: Date): String {
        return dateFormatter.format(date)
    }
}
