package com.taxigoexpress.client.trips.presentation.observers

import android.view.View
import com.taxigoexpress.client.R
import com.taxigoexpress.client.trips.domain.entities.requestEntities.TripEntity
import com.taxigoexpress.core.presentation.observers.DefaultObserverLiveData
import com.taxigoexpress.core.presentation.view.BaseLiveData
import java.io.IOException

class TripsHistoryObserver(private val mTripsLiveData: BaseLiveData<List<TripEntity>>) :
    DefaultObserverLiveData<List<TripEntity>>(mTripsLiveData) {

    override fun onStart() {
        this.mTripsLiveData.loadingObserver.value = View.VISIBLE
        super.onStart()
    }

    override fun onComplete() {
        this.mTripsLiveData.loadingObserver.value = View.GONE
    }

    override fun onNext(result: List<TripEntity>) {
        this.mTripsLiveData.customObserver.value = result
    }

    override fun onError(error: Throwable) {
        this.mTripsLiveData.loadingObserver.value = View.GONE
        when (error) {
            is IOException -> this.mTripsLiveData.showErrorObserver.value =
                R.string.no_internet_connection
            else -> this.mTripsLiveData.showExceptionObserver.value = error.localizedMessage
        }
    }

}
