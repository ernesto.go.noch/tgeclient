package com.taxigoexpress.client.trips.domain.entities.responseEntities

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.taxigoexpress.client.trips.presentation.utils.Utilities
import java.io.Serializable

data class TripsInformationResponse(
    @Expose
    @SerializedName(Utilities.ROUTES)
    val _routes: List<RoutesResponse>? = null
) : Serializable {
    data class RoutesResponse(
        @Expose
        @SerializedName(Utilities.LEGS)
        val _legs: List<LegsResponse>? = null
    )

    data class LegsResponse(
        @Expose
        @SerializedName(Utilities.DISTANCE)
        val _distance: BaseResponse? = null,

        @Expose
        @SerializedName(Utilities.DURATION)
        val _duration: BaseResponse? = null,

        @Expose
        @SerializedName(Utilities.STEPS)
        val _steps: List<StepResponse>? = null,

        @Expose
        @SerializedName("end_address")
        val _endAddress: String,

        @Expose
        @SerializedName("end_location")
        val _endLocation: StepResponse.LatLngResponse? = null,

        @Expose
        @SerializedName("start_address")
        val _startAddress: String,

        @Expose
        @SerializedName("start_location")
        val _startLocation: StepResponse.LatLngResponse? = null
    ):Serializable {
        data class BaseResponse(
            @Expose
            @SerializedName(Utilities.TEXT)
            val _text: String? = null,

            @Expose
            @SerializedName(Utilities.VALUE)
            val _value: Double? = null
        ):Serializable

        data class StepResponse(
            @Expose
            @SerializedName(Utilities.DISTANCE)
            val _distance: BaseResponse? = null,

            @Expose
            @SerializedName(Utilities.DURATION)
            val _duration: BaseResponse? = null,

            @Expose
            @SerializedName("polyline")
            val _polyline: PolylineResponse? = null


        ):Serializable {
            data class LatLngResponse(
                @Expose
                @SerializedName("lat")
                val _lat: Double? = null,

                @Expose
                @SerializedName("lng")
                val _lng: Double? = null
            ):Serializable

            data class PolylineResponse(
                @Expose
                @SerializedName("points")
                val _points: String? = null
            ):Serializable
        }
    }
}
