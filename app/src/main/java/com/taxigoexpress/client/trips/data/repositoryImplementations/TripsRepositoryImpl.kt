package com.taxigoexpress.client.trips.data.repositoryImplementations

import com.taxigoexpress.client.trips.data.datasourceImplementations.TripsDatasourceImpl
import com.taxigoexpress.client.trips.domain.entities.requestEntities.PreviewTripRequest
import com.taxigoexpress.client.trips.domain.entities.requestEntities.TripEntity
import com.taxigoexpress.client.trips.domain.entities.requestEntities.TripStatisticsRequest
import com.taxigoexpress.client.trips.domain.entities.responseEntities.PriceResponse
import com.taxigoexpress.client.trips.domain.entities.responseEntities.TripResponse
import com.taxigoexpress.client.trips.domain.entities.responseEntities.TripStatisticsResponse
import com.taxigoexpress.client.trips.domain.entities.responseEntities.TripsInformationResponse
import com.taxigoexpress.client.trips.domain.repositoryAbstractions.TripsRepository
import io.reactivex.Observable
import javax.inject.Inject

class TripsRepositoryImpl
@Inject constructor(private val tripsDatasourceImpl: TripsDatasourceImpl) : TripsRepository {

    override fun postTrip(tripEntity: TripEntity): Observable<TripEntity> =
        tripsDatasourceImpl.postTrip(tripEntity)

    override fun getTripsHistory(): Observable<List<TripEntity>> =
        this.tripsDatasourceImpl.getTripsHistory()

    override fun getTripsStatistics(request: TripStatisticsRequest): Observable<TripStatisticsResponse> =
        this.tripsDatasourceImpl.getTripsStatistics(request)

    override fun getTripTypes(): Observable<Unit> =
        tripsDatasourceImpl.getTripTypes()

    override fun getTripInformation(
        origin: String,
        destination: String
    ): Observable<TripsInformationResponse> =
        tripsDatasourceImpl.getTripInformation(origin, destination)


    override fun calculatePrice(
        request: PreviewTripRequest,
        serviceType: PriceResponse.ServiceTypeEnum
    ): Observable<PriceResponse> =
        tripsDatasourceImpl.calculatePrices(request, serviceType)

    override fun updateTrip(tripId: String): Observable<TripEntity> =
        tripsDatasourceImpl.updateTrip(tripId)

}