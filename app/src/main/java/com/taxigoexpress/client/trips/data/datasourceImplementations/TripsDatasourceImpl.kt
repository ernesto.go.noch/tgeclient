package com.taxigoexpress.client.trips.data.datasourceImplementations

import com.taxigoexpress.client.trips.data.services.MapsServices
import com.taxigoexpress.client.trips.data.services.TripServices
import com.taxigoexpress.client.trips.domain.datasourceAbstractions.TripsDataSource
import com.taxigoexpress.client.trips.domain.entities.requestEntities.PreviewTripRequest
import com.taxigoexpress.client.trips.domain.entities.requestEntities.TripEntity
import com.taxigoexpress.client.trips.domain.entities.requestEntities.TripStatisticsRequest
import com.taxigoexpress.client.trips.domain.entities.responseEntities.PriceResponse
import com.taxigoexpress.client.trips.domain.entities.responseEntities.TripResponse
import com.taxigoexpress.client.trips.domain.entities.responseEntities.TripStatisticsResponse
import com.taxigoexpress.client.trips.domain.entities.responseEntities.TripsInformationResponse
import io.reactivex.Observable
import javax.inject.Inject

class TripsDatasourceImpl
@Inject constructor(
    private val service: TripServices,
    private val mapsServices: MapsServices
) : TripsDataSource {

    override fun postTrip(tripEntity: TripEntity): Observable<TripEntity> =
        this.service.postTrip(tripEntity).flatMap {
            if (it.isSuccessful) {
                Observable.just(it.body())
            } else {
                Observable.error(Throwable(it.message()))
            }
        }


    override fun getTripsHistory(): Observable<List<TripEntity>> =
        this.service.getTripsHistory().flatMap {
            if (it.isSuccessful) {
                Observable.just(it.body())
            } else {
                Observable.error(Throwable(it.message()))
            }
        }

    override fun getTripsStatistics(request: TripStatisticsRequest): Observable<TripStatisticsResponse> =
        this.service.getTripsStatistics(request).flatMap {
            if (it.isSuccessful) {
                Observable.just(it.body())
            } else {
                Observable.error(Throwable(it.message()))
            }
        }


    override fun getTripTypes(): Observable<Unit> =
        Observable.just(Unit)

    override fun calculatePrices(
        request: PreviewTripRequest,
        serviceType: PriceResponse.ServiceTypeEnum
    ): Observable<PriceResponse> =
        service.calculatePrice(
            serviceType.type.toString(),
            request
        ).flatMap {
            if (it.isSuccessful) {
                Observable.just(
                    PriceResponse(
                        serviceType.type,
                        it.body(),
                        0
                    )
                )
            } else {
                Observable.error(Throwable(it.message()))
            }
        }

    override fun getTripInformation(
        origin: String,
        destination: String
    ): Observable<TripsInformationResponse> =
        this.mapsServices.getTripInformation(
            origin,
            destination
        ).flatMap {
            if (it.isSuccessful) {
                Observable.just(it.body())
            } else {
                Observable.error(Throwable(it.message()))
            }
        }

    override fun updateTrip(tripId: String): Observable<TripEntity> =
        this.service.updateTrip(
            tripId
        ).flatMap {
            if (it.isSuccessful) {
                Observable.just(it.body())
            } else {
                Observable.error(Throwable(it.message()))
            }
        }


}
