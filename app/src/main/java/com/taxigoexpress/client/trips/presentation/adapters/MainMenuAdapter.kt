package com.taxigoexpress.client.trips.presentation.adapters

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ia.mchaveza.kotlin_library.inflate
import com.taxigoexpress.client.R
import com.taxigoexpress.core.presentation.utils.clickEventObservable
import kotlinx.android.synthetic.main.item_main_menu.view.*

class MainMenuAdapter(
    private val optionsList: List<String>,
    private val context: Context
) :
    RecyclerView.Adapter<MainMenuAdapter.Viewholder>() {

    private var listener: MainMenuCallback? = null

    fun setMenuListener(listener: MainMenuCallback) {
        this.listener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Viewholder =
        Viewholder(parent.inflate(R.layout.item_main_menu))

    override fun getItemCount(): Int = optionsList.size

    override fun onBindViewHolder(holder: Viewholder, position: Int) {
        holder.updateItem(this.optionsList[position], context, position)
        holder.itemView.clickEventObservable().subscribe {
            val optionPicked: MainMenuCallback.MainMenuEnum = when (position) {
                0 -> MainMenuCallback.MainMenuEnum.TripRequest
                1 -> MainMenuCallback.MainMenuEnum.Coupons
                2 -> MainMenuCallback.MainMenuEnum.TripsHistory
                3 -> MainMenuCallback.MainMenuEnum.Payment
                else -> MainMenuCallback.MainMenuEnum.CloseSession
            }
            this.listener?.onGetMenuOption(optionPicked)
        }

    }

    class Viewholder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun updateItem(
            strOption: String,
            context: Context,
            position: Int
        ) {
            val drawableResource = when (position) {
                0 -> R.mipmap.ic_trip_request_foreground
                1 -> R.mipmap.ic_coupons_foreground
                2 -> R.mipmap.ic_trips_history_foreground
                3 -> R.drawable.payment_method
                else -> android.R.drawable.ic_dialog_info
            }
            itemView.tvMenuOption.text = strOption
            itemView.ivMenuOption.setImageDrawable(
                context.resources.getDrawable(
                    drawableResource,
                    context.theme
                )
            )

        }

    }
}