package com.taxigoexpress.client.trips.presentation.adapters

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.taxigoexpress.client.R
import com.taxigoexpress.client.trips.domain.entities.requestEntities.TripEntity
import com.taxigoexpress.core.presentation.utils.clickEventObservable

class TripsHistoryAdapter(private val trips: List<TripEntity>) :
    RecyclerView.Adapter<TripsHistoryAdapter.Viewholder>() {

    private var callback: TripsHistoryCallback? = null

    fun setAdapterCallback(callback: TripsHistoryCallback) {
        this.callback = callback
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Viewholder =
        Viewholder(View.inflate(parent.context, R.layout.trips_history_item, parent))


    override fun getItemCount(): Int = this.trips.size

    override fun onBindViewHolder(holder: Viewholder, position: Int) {
        holder.updateItem(this.trips[position])
        holder.itemView.clickEventObservable().subscribe {
            this.callback?.onSelectTrip(this.trips[position])
        }
    }

    class Viewholder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun updateItem(tripEntity: TripEntity) {

        }

    }

}
