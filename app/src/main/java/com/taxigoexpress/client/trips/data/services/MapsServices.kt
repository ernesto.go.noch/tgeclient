package com.taxigoexpress.client.trips.data.services

import com.taxigoexpress.client.trips.domain.entities.responseEntities.TripsInformationResponse
import com.taxigoexpress.core.BuildConfig
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface MapsServices {

    @GET(BuildConfig.GET_TRIP_INFORMATION)
    fun getTripInformation(
        @Query("origin") origin: String,
        @Query("destination") destination: String,
        @Query("sensor") sensor: Boolean = false,
        @Query("mode") mode: String = "driving"
    ): Observable<Response<TripsInformationResponse>>
}