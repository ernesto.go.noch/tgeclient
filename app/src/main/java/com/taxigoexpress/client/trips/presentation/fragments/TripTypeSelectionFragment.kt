package com.taxigoexpress.client.trips.presentation.fragments

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.taxigoexpress.client.R
import com.taxigoexpress.client.trips.domain.entities.responseEntities.PriceResponse
import com.taxigoexpress.client.trips.domain.entities.responseEntities.TripsInformationResponse
import com.taxigoexpress.client.trips.presentation.adapters.TripTypeAdapter
import com.taxigoexpress.client.trips.presentation.adapters.TripTypeAdapterCallback
import com.taxigoexpress.core.presentation.fragments.BaseFragment
import com.taxigoexpress.core.presentation.utils.clickEvent
import kotlinx.android.synthetic.main.fragment_trip_type_selection_layout.*

/**
 * A simple [Fragment] subclass.
 */
class TripTypeSelectionFragment : BaseFragment() {

    private lateinit var priceSelected: PriceResponse
    private lateinit var priceList: List<PriceResponse>
    private lateinit var tripInformation: TripsInformationResponse

    override fun getResourceLayout(): Int = R.layout.fragment_trip_type_selection_layout

    override fun initView(view: View, savedInstanceState: Bundle?) {
        this.setClickListeners()
        this.loadArguments()
    }

    private fun setClickListeners() {
        this.btnPostTrip.clickEvent().observe(viewLifecycleOwner, Observer {
            this.replaceFragment(
                PostTripFragment.newInstance(
                    priceSelected,
                    tripInformation._routes?.first()?._legs?.first()
                ),
                R.id.bottomSheet,
                true
            )
        })
    }

    private fun loadArguments() {
        this.arguments?.let {
            this.priceList = it.getSerializable(PRICES_LIST) as List<PriceResponse>
            this.tripInformation = it.getSerializable(TRIP_INFORMATION) as TripsInformationResponse
            this.loadAdapter(priceList)
        }
    }

    private fun loadAdapter(priceList: List<PriceResponse>) {

        val adapter = this.context?.let { TripTypeAdapter(priceList, it) }
        adapter?.setCallback(object : TripTypeAdapterCallback {
            override fun onSelectType(price: PriceResponse) {
                this@TripTypeSelectionFragment.priceSelected = price
            }
        })
        this.rv_trip_type_selection.adapter = adapter
        this.rv_trip_type_selection.layoutManager = LinearLayoutManager(this.context)
    }

    companion object {
        private const val TRIP_INFORMATION = "trip.information"
        private const val PRICES_LIST = "price.list"

        fun newInstance(prices: List<PriceResponse>, tripsInformation: TripsInformationResponse) =
            TripTypeSelectionFragment().apply {
                arguments = bundleOf(
                    PRICES_LIST to prices,
                    TRIP_INFORMATION to tripsInformation
                )
            }
    }

}
