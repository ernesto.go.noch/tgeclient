package com.taxigoexpress.client.trips.presentation.adapters

import com.taxigoexpress.client.trips.domain.entities.requestEntities.TripEntity

interface TripsHistoryCallback {
    fun onSelectTrip(tripEntity: TripEntity)

}
