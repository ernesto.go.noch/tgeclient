package com.taxigoexpress.client.trips.presentation.viewmodel.implementation

import com.taxigoexpress.client.trips.domain.entities.requestEntities.TripEntity
import com.taxigoexpress.client.trips.domain.useCases.GetTripsHistoryUseCase
import com.taxigoexpress.client.trips.presentation.observers.TripsHistoryObserver
import com.taxigoexpress.client.trips.presentation.viewmodel.abstraction.TripsHistoryViewModel
import com.taxigoexpress.core.presentation.view.BaseLiveData
import com.taxigoexpress.core.presentation.viewmodel.BaseViewModelLiveData
import javax.inject.Inject

class TripsHistoryViewModelImpl
@Inject constructor(private val getTripsHistoryUseCase: GetTripsHistoryUseCase) :
    BaseViewModelLiveData<List<TripEntity>>(),
    TripsHistoryViewModel<List<TripEntity>> {

    private val mTripsLiveData by lazy { this.getCustomLiveData() }


    override fun getTripsHistory(passId: String) {
        this.getTripsHistoryUseCase.execute(
            TripsHistoryObserver(mTripsLiveData),
            passId
        )
    }

    override fun observeTripsHistory(): BaseLiveData<List<TripEntity>> =
        this.mTripsLiveData

    override fun observeResult(): BaseLiveData<List<TripEntity>> =
        this.mTripsLiveData

    override fun onCleared() {
        this.getTripsHistoryUseCase.dispose()
        super.onCleared()
    }


}