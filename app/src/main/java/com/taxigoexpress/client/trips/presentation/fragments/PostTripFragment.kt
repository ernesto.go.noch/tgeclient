package com.taxigoexpress.client.trips.presentation.fragments


import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.taxigoexpress.client.R
import com.taxigoexpress.client.trips.domain.entities.requestEntities.TripEntity
import com.taxigoexpress.client.trips.domain.entities.responseEntities.PriceResponse
import com.taxigoexpress.client.trips.domain.entities.responseEntities.TripsInformationResponse
import com.taxigoexpress.client.trips.presentation.components.DaggerTripsComponent
import com.taxigoexpress.client.trips.presentation.components.TripsComponent
import com.taxigoexpress.client.trips.presentation.modules.TripsModule
import com.taxigoexpress.client.trips.presentation.viewmodel.abstraction.PostTripViewModel
import com.taxigoexpress.core.getApplicationComponent
import com.taxigoexpress.core.presentation.fragments.BaseFragment
import kotlinx.android.synthetic.main.fragment_post_trip_layout.*
import org.jetbrains.anko.support.v4.runOnUiThread
import javax.inject.Inject


/**
 * A simple [Fragment] subclass.
 */
class PostTripFragment : BaseFragment() {

    private val tripsComponent: TripsComponent by lazy {
        DaggerTripsComponent.builder()
            .applicationComponent(activity?.getApplicationComponent())
            .tripsModule(TripsModule())
            .build()
    }

    @Inject
    lateinit var postTripViewModel: PostTripViewModel<TripEntity>

    private var tripInformation: TripsInformationResponse.LegsResponse? = null
    private lateinit var price: PriceResponse


    override fun getResourceLayout(): Int = R.layout.fragment_post_trip_layout

    override fun initView(view: View, savedInstanceState: Bundle?) {
        this.tripsComponent.inject(this)
        this.loadArguments()
        this.observeResults()
    }

    private fun loadArguments() {
        this.arguments?.let {
            this.price = it.getSerializable(PRICE_RESPONSE) as PriceResponse
            this.tripInformation =
                it.getSerializable(TRIP_INFORMATION) as TripsInformationResponse.LegsResponse?
            this.tripInformation?.let { information ->
                this.postTripViewModel.postTrip(price, information)
            }
        }
    }

    private fun observeResults() {
        with(this.postTripViewModel.observablePostTrip()) {
            this.customObserver.observe(this@PostTripFragment, Observer {
                it?.let {
                    it._id?.let { tripId ->
                        this@PostTripFragment.runOnUiThread {
                            postTripViewModel.updateTrip(tripId = tripId.toString())
                        }
                    }
                }
            })

            this.loadingObserver.observe(this@PostTripFragment, Observer {

            })

            this.showExceptionObserver.observe(this@PostTripFragment, Observer {
                this@PostTripFragment.showAlert(it ?: "")
            })

            this.showErrorObserver.observe(this@PostTripFragment, Observer {
                this@PostTripFragment.showAlertWithResource(it ?: 0)
            })
        }

        with(this.postTripViewModel.observableTrackTrip()) {
            this.customObserver.observe(this@PostTripFragment, Observer {
                it?.let {
                    updateTrip(it)
                }
            })

            this.loadingObserver.observe(this@PostTripFragment, Observer {

            })

            this.showExceptionObserver.observe(this@PostTripFragment, Observer {
                this@PostTripFragment.showAlert(it ?: "")
            })

            this.showErrorObserver.observe(this@PostTripFragment, Observer {
                this@PostTripFragment.showAlertWithResource(it ?: 0)
            })
        }
    }

    private fun updateTrip(entity: TripEntity) {
        tvRequestMessage.text = String.format(
            "%s...",
            entity.status.toLowerCase().capitalize()
        )
    }

    companion object {
        private const val PRICE_RESPONSE = "price.response"
        private const val TRIP_INFORMATION = "trip.information"

        fun newInstance(
            priceResponse: PriceResponse,
            tripsInformation: TripsInformationResponse.LegsResponse?
        ) = PostTripFragment().apply {
            arguments = bundleOf(
                PRICE_RESPONSE to priceResponse,
                TRIP_INFORMATION to tripsInformation
            )
        }
    }
}
