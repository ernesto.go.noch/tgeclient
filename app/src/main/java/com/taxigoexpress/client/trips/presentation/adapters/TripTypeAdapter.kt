package com.taxigoexpress.client.trips.presentation.adapters

import android.content.Context
import android.os.Build
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ia.mchaveza.kotlin_library.inflate
import com.ia.mchaveza.kotlin_library.setDrawableBackground
import com.taxigoexpress.client.R
import com.taxigoexpress.client.trips.domain.entities.responseEntities.PriceResponse
import com.taxigoexpress.client.trips.presentation.utils.Utilities
import com.taxigoexpress.core.presentation.utils.clickEventObservable
import kotlinx.android.synthetic.main.item_trip_type_layout.view.*

class TripTypeAdapter(
    private val prices: List<PriceResponse>,
    private val context: Context
) :
    RecyclerView.Adapter<TripTypeAdapter.ViewHolder>() {

    private var callback: TripTypeAdapterCallback? = null
    private var adapterPosition: Int = -1

    fun setPosition(currentPosition: Int) {
        this.adapterPosition = currentPosition
        this.notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent.inflate(R.layout.item_trip_type_layout))
    }

    override fun getItemCount(): Int = prices.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val price = this.prices[position]
        holder.updateItem(this.context, this.adapterPosition == position, price)
        holder.itemView.cl_main_item_trip_type.clickEventObservable().subscribe {
            this.setPosition(holder.adapterPosition)
            this.callback?.onSelectType(price)
        }
    }

    fun setCallback(callback: TripTypeAdapterCallback) {
        this.callback = callback
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun updateItem(
            context: Context,
            isSelected: Boolean,
            price: PriceResponse
        ) {
            if (isSelected) {
                itemView.cl_main_item_trip_type.setDrawableBackground(R.drawable.trip_type_item_background)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    itemView.tvEstimatedArrived.setTextColor(
                        context.resources.getColor(
                            R.color.colorPrimary,
                            context.theme
                        )
                    )
                }
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    itemView.cl_main_item_trip_type.setBackgroundColor(
                        context.resources.getColor(
                            R.color.white,
                            context.theme
                        )
                    )
                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    itemView.tvEstimatedArrived.setTextColor(
                        context.resources.getColor(
                            R.color.indicator_grey,
                            context.theme
                        )
                    )
                }
            }
            when (price.serviceType) {
                PriceResponse.ServiceTypeEnum.Basic -> itemView.tvTripType.text =
                    context.getString(R.string.basic_service)
                PriceResponse.ServiceTypeEnum.Premium -> itemView.tvTripType.text =
                    context.getString(R.string.premium_service)
                else -> itemView.tvTripType.text = context.getString(R.string.deluxe_service)
            }
            itemView.tvTripCurrentPrice.text = Utilities.getCostFormat(price._price?.toDouble())
        }

    }
}