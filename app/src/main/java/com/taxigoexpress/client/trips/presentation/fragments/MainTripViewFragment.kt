package com.taxigoexpress.client.trips.presentation.fragments

import android.Manifest
import android.annotation.SuppressLint
import android.content.ContentValues.TAG
import android.content.res.Resources
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.FrameLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.*
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.ia.mchaveza.kotlin_library.*
import com.taxigoexpress.client.HomeActivity
import com.taxigoexpress.client.R
import com.taxigoexpress.client.trips.domain.entities.responseEntities.PriceResponse
import com.taxigoexpress.client.trips.domain.entities.responseEntities.TripsInformationResponse
import com.taxigoexpress.client.trips.presentation.components.DaggerTripsComponent
import com.taxigoexpress.client.trips.presentation.components.TripsComponent
import com.taxigoexpress.client.trips.presentation.modules.TripsModule
import com.taxigoexpress.client.trips.presentation.utils.Utilities.decodePoly
import com.taxigoexpress.client.trips.presentation.viewmodel.abstraction.MainTripViewModel
import com.taxigoexpress.core.getApplicationComponent
import com.taxigoexpress.core.presentation.fragments.BaseFragment
import com.taxigoexpress.core.presentation.fragments.LoaderFragment
import com.taxigoexpress.core.presentation.utils.clickEvent
import com.taxigoexpress.places.PlaceAPI
import com.taxigoexpress.places.PlaceAPI.Companion.TEST_API_KEY
import com.taxigoexpress.places.listener.OnPlacesDetailsListener
import com.taxigoexpress.places.model.Place
import com.taxigoexpress.places.model.PlaceDetails
import kotlinx.android.synthetic.main.fragment_main_trip_view_layout.*
import kotlinx.android.synthetic.main.origin_destination_layout.*
import kotlinx.android.synthetic.main.pick_trip_layout.*
import javax.inject.Inject
import kotlin.math.absoluteValue

/**
 * A simple [Fragment] subclass.
 *
 */
class MainTripViewFragment : BaseFragment(),
    OnMapReadyCallback,
    PermissionCallback,
    TrackingManagerLocationCallback {

    private lateinit var tripInformation: TripsInformationResponse
    @Inject
    lateinit var mainTripViewModel: MainTripViewModel<List<PriceResponse>>

    private var bottomSheetB: BottomSheetBehavior<FrameLayout>? = null

    private val tripsComponent: TripsComponent by lazy {
        DaggerTripsComponent.builder()
            .applicationComponent(activity?.getApplicationComponent())
            .tripsModule(TripsModule())
            .build()
    }

    private val loaderDialog by lazy { LoaderFragment.newInstance(getString(R.string.loading)) }

    private val searchPlaces = SearchPlacesFragment.newInstance()

    override fun getResourceLayout(): Int = R.layout.fragment_main_trip_view_layout

    override fun initView(view: View, savedInstanceState: Bundle?) {
        this.tripsComponent.inject(this)
//        this.tripsViewModel.getTripTypes()
        this.mvMainTrip.onCreate(savedInstanceState)
        this.mvMainTrip.getMapAsync(this)
        this.observeResults()
    }

    private var mGoogleMap: GoogleMap? = null
    private var currentLocationFounded: Location? = null

    private val trackingManager: TrackingManager by lazy {
        TrackingManager(this.context!!)
    }

    private val permissionManager by lazy { PermissionManager(activity!!, this) }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        this.mvMainTrip.onSaveInstanceState(outState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.setClickListeners()
        this.configureToolbar()
    }

    private fun configureToolbar() {
        (this.activity as HomeActivity).hideToolbar()
    }

    private fun setClickListeners() {

        this.bottomSheetB = BottomSheetBehavior.from(bottomSheet)
        this.bottomSheetB?.state = BottomSheetBehavior.STATE_HIDDEN

        val placesApi = this.activity?.let {
            PlaceAPI.Builder()
                .apiKey(TEST_API_KEY)
                .build(it)
        }

        this.searchViewLayout.clickEvent().observe(viewLifecycleOwner, Observer {
            this.currentLocationFounded?.let { location -> searchPlaces.setFoundedLocation(location) }
            searchPlaces.show(
                this.childFragmentManager,
                MainTripViewFragment::class.java.name
            )
        })

        this.ivSetCurrentLocation.clickEvent().observe(viewLifecycleOwner, Observer {
            this.currentLocationFounded?.let { location -> this.addMarkerToMap(location) }
        })

        this.fabMenu.clickEvent().observe(this.viewLifecycleOwner, Observer {
            (this.activity as HomeActivity).openDrawer()
        })

        this.ivBackMap.clickEvent().observe(viewLifecycleOwner, Observer {
            bottomSheetB?.state = BottomSheetBehavior.STATE_HIDDEN
            this.cvBackTrip.gone()
            this.layoutCardOriginDestination.gone()
            this.mGoogleMap?.clear()
            this.currentLocationFounded?.let { currentLocation ->
                this.addMarkerToMap(
                    currentLocation
                )
            }
        })

        this.searchPlaces.setCallback(object : SearchPlacesCallback {
            override fun onSelectedPlaces(origin: Place, destination: Place) {
                this@MainTripViewFragment.loaderDialog.show(
                    childFragmentManager,
                    MainTripViewFragment::class.java.name
                )
                placesApi?.fetchPlaceDetails(origin.id, object : OnPlacesDetailsListener {
                    override fun onPlaceDetailsFetched(originDetails: PlaceDetails) {
                        placesApi.fetchPlaceDetails(
                            destination.id,
                            object : OnPlacesDetailsListener {
                                override fun onPlaceDetailsFetched(destinationDetails: PlaceDetails) {
                                    this@MainTripViewFragment.activity?.runOnUiThread {
                                        tvCardOrigin.text = originDetails.name
                                        tvCardDestination.text = destinationDetails.name
                                        layoutCardOriginDestination.visible()
                                        this@MainTripViewFragment.mainTripViewModel.getTripInformation(
                                            Pair(
                                                originDetails,
                                                destinationDetails
                                            )
                                        )
                                    }
                                }

                                override fun onError(errorMessage: String) {
                                    this@MainTripViewFragment.showAlert(errorMessage)
                                    this@MainTripViewFragment.loaderDialog.dismiss()
                                }
                            })
                    }

                    override fun onError(errorMessage: String) {
                        this@MainTripViewFragment.showAlert(errorMessage)
                        this@MainTripViewFragment.loaderDialog.dismiss()
                    }

                })
            }

        })
    }

    override fun onPermissionDenied(permission: String) {
    }

    override fun onPermissionGranted(permission: String) {
        this.setCurrentLocationOnMap()
    }

    override fun onMapReady(p0: GoogleMap?) {
//        p0?.addMarker(MarkerOptions().position(LatLng(0.0, 0.0)).title("Marker"))
        this.mGoogleMap = p0
        this.mGoogleMap?.isMyLocationEnabled = false
        this.mGoogleMap?.uiSettings?.isMyLocationButtonEnabled = false
        this.setCurrentLocationOnMap()
        this.changeMapTheme()
        this.observeResults()
    }

    private fun changeMapTheme() {
        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            this.mGoogleMap?.setMapStyle(
                MapStyleOptions.loadRawResourceStyle(
                    this.context, R.raw.x_gmap_style_variant1
                )
            )

        } catch (e: Resources.NotFoundException) {
            Log.e(TAG, "Can't find style. Error: ", e)
        }

    }

    private fun setCurrentLocationOnMap() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!this.permissionManager.requestSinglePermission(Manifest.permission.ACCESS_FINE_LOCATION)) {
                this.startLocating()
            }
        } else {
            if (permissionManager.permissionGranted(Manifest.permission.ACCESS_FINE_LOCATION)) {
                this.startLocating()
            }
        }
    }

    @SuppressLint("MissingPermission")
    private fun startLocating() {
        this.mGoogleMap?.isMyLocationEnabled = true
        this.mGoogleMap?.uiSettings?.isMyLocationButtonEnabled = true
//        if (this.trackingManager.areLocationServicesEnabled()) {
        this.trackingManager.startLocationUpdates(this, 2000, 1000)
//        }
//
    }

    override fun onLocationHasChanged(location: Location) {
        this.currentLocationFounded?.let {
            val diffLAtitude = (it.latitude - location.latitude).absoluteValue
            val diffLongitude = (it.longitude - location.longitude).absoluteValue
            if (diffLAtitude >= 10 || diffLongitude >= 10) {
                this.addMarkerToMap(location)
            }
        } ?: kotlin.run {
            this.addMarkerToMap(location)
        }
        this.currentLocationFounded = location
    }

    override fun onLocationHasChangedError(error: Exception) {
        this.trackingManager.stopLocationUpdates()
    }

    private fun addMarkerToMap(location: Location) {
        this.mGoogleMap?.moveCamera(
            CameraUpdateFactory.newLatLngZoom(
                LatLng(location.latitude, location.longitude),
                18f
            )
        )
        this.mGoogleMap?.animateCamera(CameraUpdateFactory.zoomTo(15f))
    }

    private fun observeResults() {

        with(this.mainTripViewModel.observableTripInformation()) {

            this.customObserver.observe(this@MainTripViewFragment, Observer {
                it?.let {
                    this@MainTripViewFragment.tripInformation = it
                    paintMapRoute(it)
                }
            })

            this.loadingObserver.observe(this@MainTripViewFragment, Observer {
                if (it == View.GONE) {
                    this@MainTripViewFragment.loaderDialog.dismiss()
                }
            })

            this.showExceptionObserver.observe(this@MainTripViewFragment, Observer {
                this@MainTripViewFragment.showAlert(it ?: "")
            })

            this.showErrorObserver.observe(this@MainTripViewFragment, Observer {
                this@MainTripViewFragment.showAlertWithResource(it ?: 0)
            })
        }

        with(this.mainTripViewModel.observablePrice()) {
            this.customObserver.observe(this@MainTripViewFragment, Observer {
                it?.let {
                    showTripCost(it)
                }
            })

            this.loadingObserver.observe(this@MainTripViewFragment, Observer {
                if (it == View.GONE) {
                    this@MainTripViewFragment.loaderDialog.dismiss()
                }
            })

            this.showExceptionObserver.observe(this@MainTripViewFragment, Observer {
                this@MainTripViewFragment.showAlert(it ?: "")
            })

            this.showErrorObserver.observe(this@MainTripViewFragment, Observer {
                this@MainTripViewFragment.showAlertWithResource(it ?: 0)
            })
        }
    }

    private fun paintMapRoute(tripInformation: TripsInformationResponse) {
        this.mGoogleMap?.clear()
        val latLongB = LatLngBounds.Builder()
        val origin = tripInformation._routes?.first()?._legs?.first()?._startLocation!!
        val destination = tripInformation._routes.first()._legs?.first()?._endLocation!!
        val originPlace = origin._lat?.let { origin._lng?.let { lng -> LatLng(it, lng) } }
        val destinationPlace = destination._lat?.let {
            destination._lng?.let { lng ->
                LatLng(
                    it,
                    lng
                )
            }
        }
        val originMarker = BitmapDescriptorFactory.fromResource(R.drawable.ic_start_pin)
        val destinationMarker = BitmapDescriptorFactory.fromResource(R.drawable.ic_end_pin)
        this.mGoogleMap!!.addMarker(
            originPlace?.let { MarkerOptions().position(it).icon(originMarker) })
        this.mGoogleMap!!.addMarker(
            destinationPlace?.let { MarkerOptions().position(it).icon(destinationMarker) })
        // Declare polyline object and set up color and width
        val options = PolylineOptions()
        activity?.applicationContext?.let { context ->
            ContextCompat.getColor(context, R.color.colorPrimary)
        }?.let { color ->
            options.color(color)
        }
        options.width(12f)
        val points = tripInformation._routes.first()._legs?.first()?._steps
        // For every element in the JsonArray, decode the polyline string and pass all points to a List
        val polypts = points!!.flatMap { decodePoly(it._polyline?._points!!) }

        // Add  points to polyline and bounds
        options.add(originPlace)
        latLongB.include(originPlace)
        for (point in polypts) {
            options.add(point)
            latLongB.include(point)
        }
        options.add(destinationPlace)
        latLongB.include(destinationPlace)
        // build bounds
        val bounds = latLongB.build()
        // add polyline to the map
        mGoogleMap?.addPolyline(options)
        // show map with route centered
        mGoogleMap?.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100))

        tripInformation._routes.first()._legs?.let {
            this.mainTripViewModel.calculatePrice(it.first())
        }
    }

    private fun showTripCost(prices: List<PriceResponse>) {
        val tripTypeSelectionFragment = TripTypeSelectionFragment.newInstance(
            prices,
            this.tripInformation
        )
        this.replaceChildFragment(
            tripTypeSelectionFragment,
            R.id.bottomSheet,
            false
        )

        this.cvBackTrip.visible()
        this.bottomSheetB?.state = BottomSheetBehavior.STATE_EXPANDED
    }

    override fun onResume() {
        super.onResume()
        this.mvMainTrip?.onResume()
    }

    override fun onPause() {
        super.onPause()
        this.mvMainTrip?.onPause()
    }

    override fun onStart() {
        super.onStart()
        this.mvMainTrip?.onStart()
    }

    override fun onStop() {
        super.onStop()
        this.mvMainTrip?.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
        this.mvMainTrip?.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        this.mvMainTrip?.onLowMemory()
    }
}
