package com.taxigoexpress.client.trips.domain.entities.requestEntities

import com.google.gson.annotations.SerializedName

data class TripStatisticsRequest(
    @field:SerializedName("fechaInicio")
    val _startDate: String? = null,

    @field:SerializedName("fechaFin")
    val _endDate: String? = null
) {
    val startDate: String
        get() = _startDate ?: ""

    val endDate: String
        get() = _endDate ?: ""
}
