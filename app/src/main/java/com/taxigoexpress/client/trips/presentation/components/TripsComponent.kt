package com.taxigoexpress.client.trips.presentation.components

import com.taxigoexpress.client.trips.presentation.fragments.MainTripViewFragment
import com.taxigoexpress.client.trips.presentation.fragments.PostTripFragment
import com.taxigoexpress.core.presentation.components.ApplicationComponent
import com.taxigoexpress.core.presentation.scopes.FragmentScope
import com.taxigoexpress.client.trips.presentation.fragments.TripTypeSelectionFragment
import com.taxigoexpress.client.trips.presentation.fragments.TripsHistoryFragment
import com.taxigoexpress.client.trips.presentation.modules.TripsModule
import dagger.Component

@FragmentScope
@Component(
    dependencies = [ApplicationComponent::class],
    modules = [TripsModule::class]
)
interface TripsComponent {
    fun inject(mainTripViewFragment: MainTripViewFragment)
    fun inject(tripsHistoryFragment: TripsHistoryFragment)
    fun inject(postTripFragment: PostTripFragment)
}