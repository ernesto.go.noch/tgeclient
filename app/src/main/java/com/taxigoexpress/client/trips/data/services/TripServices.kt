package com.taxigoexpress.client.trips.data.services

import com.taxigoexpress.client.trips.domain.entities.requestEntities.PreviewTripRequest
import com.taxigoexpress.client.trips.domain.entities.responseEntities.TripResponse
import com.taxigoexpress.client.trips.domain.entities.requestEntities.TripEntity
import com.taxigoexpress.client.trips.domain.entities.requestEntities.TripStatisticsRequest
import com.taxigoexpress.client.trips.domain.entities.responseEntities.TripStatisticsResponse
import com.taxigoexpress.core.BuildConfig
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.*

interface TripServices {

    @POST(BuildConfig.POST_TRIP)
    fun postTrip(
        @Body tripEntity: TripEntity
    ): Observable<Response<TripEntity>>

    @POST(BuildConfig.GET_TRIPS_STATISTICS)
    fun getTripsStatistics(
        @Body getTripStatisticsRequest: TripStatisticsRequest
    ): Observable<Response<TripStatisticsResponse>>

    @GET(BuildConfig.POST_TRIP)
    fun getTripsHistory():
            Observable<Response<List<TripEntity>>>

    @PUT(BuildConfig.GET_PREVIEW_COST)
    fun calculatePrice(
        @Path("serviceType") type: String,
        @Body tripRequest: PreviewTripRequest
    ): Observable<Response<String>>

    @GET(BuildConfig.UPDATE_TRIP)
    fun updateTrip(
        @Path("id") tripId: String
    ): Observable<Response<TripEntity>>

}