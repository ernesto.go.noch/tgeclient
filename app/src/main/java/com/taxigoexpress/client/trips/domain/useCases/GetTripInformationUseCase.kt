package com.taxigoexpress.client.trips.domain.useCases

import com.taxigoexpress.client.trips.domain.entities.responseEntities.TripsInformationResponse
import com.taxigoexpress.client.trips.domain.repositoryAbstractions.TripsRepository
import com.taxigoexpress.core.domain.executors.PostExecutionThread
import com.taxigoexpress.core.domain.executors.ThreadExecutor
import com.taxigoexpress.core.domain.useCases.UseCase
import com.taxigoexpress.places.model.PlaceDetails
import io.reactivex.Observable
import javax.inject.Inject

class GetTripInformationUseCase
@Inject constructor(
    private val repository: TripsRepository,
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread
) :
    UseCase<TripsInformationResponse, Pair<PlaceDetails, PlaceDetails>>(
        threadExecutor,
        postExecutionThread
    ) {


    override fun createObservable(params: Pair<PlaceDetails, PlaceDetails>): Observable<TripsInformationResponse> =
        this.repository.getTripInformation(
            getLatLngFormat(params.first),
            getLatLngFormat(params.second)
        )

    private fun getLatLngFormat(placeDetails: PlaceDetails): String =
        String.format(
            "%s,%s",
            placeDetails.lat,
            placeDetails.lng
        )
}