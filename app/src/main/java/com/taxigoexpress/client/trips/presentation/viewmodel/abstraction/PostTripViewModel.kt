package com.taxigoexpress.client.trips.presentation.viewmodel.abstraction

import com.taxigoexpress.client.trips.domain.entities.responseEntities.PriceResponse
import com.taxigoexpress.client.trips.domain.entities.responseEntities.TripsInformationResponse
import com.taxigoexpress.core.presentation.view.BaseLiveData
import com.taxigoexpress.core.presentation.viewmodel.PresenterLiveData

interface PostTripViewModel<T> : PresenterLiveData<T> {
    fun postTrip(
        priceResponse: PriceResponse,
        tripInformation: TripsInformationResponse.LegsResponse
    )

    fun observablePostTrip(): BaseLiveData<T>
    fun updateTrip(tripId: String)
    fun observableTrackTrip(): BaseLiveData<T>
}