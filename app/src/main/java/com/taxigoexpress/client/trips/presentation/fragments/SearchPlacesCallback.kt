package com.taxigoexpress.client.trips.presentation.fragments

import com.taxigoexpress.places.model.Place

interface SearchPlacesCallback {
    fun onSelectedPlaces(origin: Place, destination: Place)

}
