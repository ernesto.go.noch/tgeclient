package com.taxigoexpress.client.trips.presentation.viewmodel.implementation

import androidx.lifecycle.MutableLiveData
import com.taxigoexpress.client.trips.domain.entities.responseEntities.PriceResponse
import com.taxigoexpress.client.trips.domain.entities.responseEntities.TripsInformationResponse
import com.taxigoexpress.client.trips.domain.useCases.CalculatePriceUseCase
import com.taxigoexpress.client.trips.domain.useCases.GetTripInformationUseCase
import com.taxigoexpress.core.presentation.view.BaseLiveData
import com.taxigoexpress.core.presentation.viewmodel.BaseViewModelLiveData
import com.taxigoexpress.client.trips.domain.useCases.PostTripUseCase
import com.taxigoexpress.client.trips.presentation.observers.PricesObserver
import com.taxigoexpress.client.trips.presentation.observers.TripInformationObserver
import com.taxigoexpress.client.trips.presentation.viewmodel.abstraction.MainTripViewModel
import com.taxigoexpress.places.model.PlaceDetails
import javax.inject.Inject

class MainTripViewModelImpl
@Inject constructor(
    private val calculatePriceUseCase: CalculatePriceUseCase,
    private val getTripInformationUseCase: GetTripInformationUseCase,
    private val postTripUseCase: PostTripUseCase
) : BaseViewModelLiveData<List<PriceResponse>>(), MainTripViewModel<List<PriceResponse>> {

    private val mPricesLiveData by lazy { this.getCustomLiveData() }
    private val mTripInformationLiveData by lazy {
        this.buildLiveData(
            MutableLiveData<TripsInformationResponse>()
        )
    }

    override fun calculatePrice(request: TripsInformationResponse.LegsResponse) =
        this.calculatePriceUseCase.execute(
            PricesObserver(
                this.mPricesLiveData
            ), request
        )

    override fun observablePrice(): BaseLiveData<List<PriceResponse>> =
        this.mPricesLiveData

    override fun getTripInformation(params: Pair<PlaceDetails, PlaceDetails>) =
        this.getTripInformationUseCase.execute(
            TripInformationObserver(this.mTripInformationLiveData),
            params
        )


    override fun observableTripInformation(): BaseLiveData<TripsInformationResponse> =
        this.mTripInformationLiveData

    override fun observeResult(): BaseLiveData<List<PriceResponse>> =
        this.mPricesLiveData

    override fun onCleared() {
        this.calculatePriceUseCase.dispose()
        this.getTripInformationUseCase.dispose()
        this.postTripUseCase.dispose()
        super.onCleared()
    }
}