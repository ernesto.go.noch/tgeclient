package com.taxigoexpress.client.trips.presentation.fragments

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.taxigoexpress.client.HomeActivity
import com.taxigoexpress.client.R
import com.taxigoexpress.client.trips.domain.entities.requestEntities.TripEntity
import com.taxigoexpress.client.trips.presentation.adapters.TripsHistoryAdapter
import com.taxigoexpress.client.trips.presentation.adapters.TripsHistoryCallback
import com.taxigoexpress.client.trips.presentation.components.DaggerTripsComponent
import com.taxigoexpress.client.trips.presentation.modules.TripsModule
import com.taxigoexpress.client.trips.presentation.viewmodel.abstraction.TripsHistoryViewModel
import com.taxigoexpress.core.getApplicationComponent
import com.taxigoexpress.core.presentation.fragments.BaseFragment
import com.taxigoexpress.core.presentation.fragments.LoaderFragment
import com.taxigoexpress.core.presentation.utils.Constants
import kotlinx.android.synthetic.main.fragment_trips_history_layout.*
import javax.inject.Inject

class TripsHistoryFragment : BaseFragment() {

    @Inject
    lateinit var tripsHistoryViewModel: TripsHistoryViewModel<List<TripEntity>>

    private val loaderDialog by lazy { LoaderFragment.newInstance(getString(com.taxigoexpress.register.R.string.loading)) }

    private val component by lazy {
        DaggerTripsComponent.builder()
            .applicationComponent(activity?.getApplicationComponent())
            .tripsModule(TripsModule())
            .build()
    }

    override fun getResourceLayout(): Int = R.layout.fragment_trips_history_layout

    override fun initView(view: View, savedInstanceState: Bundle?) {
        this.component.inject(this)
        keyStoreManager?.retrieveDataWithSafeMode(
            Constants.PASS_ID
        )?.let { this.tripsHistoryViewModel.getTripsHistory(it) }
        this.configureToolbar()
        this.observeResults()
    }

    private fun configureToolbar() {
        this.showToolbar()
        this.activity?.let {
            (it as HomeActivity).changeToolbarTitle(
                this.getString(R.string.trips_history_title)
            )
        }
    }

    private fun observeResults() {
        with(this.tripsHistoryViewModel.observeTripsHistory()) {
            this.customObserver.observe(this@TripsHistoryFragment, Observer {
                this@TripsHistoryFragment.loadAdapter(it)
            })

            this.loadingObserver.observe(this@TripsHistoryFragment, Observer {
                if (it == View.VISIBLE) {
                    loaderDialog.show(
                        this@TripsHistoryFragment.childFragmentManager,
                        LoaderFragment::class.java.name
                    )
                } else {
                    loaderDialog.dismiss()
                }
            })

            this.showExceptionObserver.observe(this@TripsHistoryFragment, Observer {
                this@TripsHistoryFragment.showAlert(it ?: "")
            })

            this.showErrorObserver.observe(this@TripsHistoryFragment, Observer {
                this@TripsHistoryFragment.showAlertWithResource(it ?: 0)
            })
        }
    }

    private fun loadAdapter(trips: List<TripEntity>) {
        val adapter = TripsHistoryAdapter(trips)
        this.rvTripsHistory.adapter = adapter
        this.rvTripsHistory.layoutManager = LinearLayoutManager(this.context)
        adapter.setAdapterCallback(object : TripsHistoryCallback {
            override fun onSelectTrip(tripEntity: TripEntity) {
                this@TripsHistoryFragment.replaceFragment(
                    TripDetailFragment.newInstance(tripEntity),
                    R.id.home_container,
                    true
                )
            }
        })
    }

    companion object {
        fun newInstance() = TripsHistoryFragment()
    }
}