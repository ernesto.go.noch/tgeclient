package com.taxigoexpress.client.trips.presentation.fragments


import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.taxigoexpress.client.HomeActivity
import com.taxigoexpress.client.R
import com.taxigoexpress.client.trips.domain.entities.requestEntities.TripEntity
import com.taxigoexpress.core.presentation.fragments.BaseFragment

/**
 * A simple [Fragment] subclass.
 */
class TripDetailFragment : BaseFragment() {

    private lateinit var tripEntity: TripEntity

    override fun getResourceLayout(): Int = R.layout.fragment_trip_detail_layout

    override fun initView(view: View, savedInstanceState: Bundle?) {
        this.loadArguments()
        this.configureToolbar()
    }

    private fun configureToolbar() {
        this.showToolbar()
        this.activity?.let {
            (it as HomeActivity).changeToolbarTitle(
                this.getString(R.string.trip_details_title)
            )
        }
    }

    private fun loadArguments() {
        this.arguments?.let {
            this.tripEntity = it.getSerializable(TRIP_ENTITY) as TripEntity
        }
    }

    companion object {
        private const val TRIP_ENTITY = "trip.entity"

        fun newInstance(tripEntity: TripEntity) =
            TripDetailFragment().apply {
                this.arguments.apply {
                    tripEntity to TRIP_ENTITY
                }
            }
    }


}
