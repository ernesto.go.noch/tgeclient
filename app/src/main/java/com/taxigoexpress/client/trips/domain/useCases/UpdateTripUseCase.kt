package com.taxigoexpress.client.trips.domain.useCases

import com.taxigoexpress.client.trips.domain.entities.requestEntities.TripEntity
import com.taxigoexpress.client.trips.domain.repositoryAbstractions.TripsRepository
import com.taxigoexpress.core.domain.executors.PostExecutionThread
import com.taxigoexpress.core.domain.executors.ThreadExecutor
import com.taxigoexpress.core.domain.useCases.UseCase
import io.reactivex.Observable
import javax.inject.Inject

class UpdateTripUseCase
@Inject constructor(
    private val repository: TripsRepository,
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread
) :
    UseCase<TripEntity, String>(threadExecutor, postExecutionThread) {

    override fun createObservable(params: String): Observable<TripEntity> =
        this.repository.updateTrip(params)

}
