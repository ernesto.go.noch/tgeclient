package com.taxigoexpress.client.trips.presentation.viewmodel.implementation

import com.taxigoexpress.client.trips.domain.entities.requestEntities.TripEntity
import com.taxigoexpress.client.trips.domain.entities.responseEntities.PriceResponse
import com.taxigoexpress.client.trips.domain.entities.responseEntities.TripsInformationResponse
import com.taxigoexpress.client.trips.domain.useCases.PostTripUseCase
import com.taxigoexpress.client.trips.domain.useCases.UpdateTripUseCase
import com.taxigoexpress.client.trips.presentation.observers.PostTripObserver
import com.taxigoexpress.client.trips.presentation.viewmodel.abstraction.PostTripViewModel
import com.taxigoexpress.core.presentation.view.BaseLiveData
import com.taxigoexpress.core.presentation.viewmodel.BaseViewModelLiveData
import javax.inject.Inject

class PostTripViewModelImpl
@Inject constructor(
    private val postTripUseCase: PostTripUseCase,
    private val updateTripUseCase: UpdateTripUseCase
) : BaseViewModelLiveData<TripEntity>(), PostTripViewModel<TripEntity> {

    private val mTripLiveData by lazy { this.getCustomLiveData() }
    private val mUpdateLiveData by lazy { this.getCustomLiveData() }

    override fun postTrip(
        priceResponse: PriceResponse,
        tripInformation: TripsInformationResponse.LegsResponse
    ) = postTripUseCase.execute(
        PostTripObserver(this.mTripLiveData),
        Pair(priceResponse, tripInformation)
    )

    override fun observablePostTrip(): BaseLiveData<TripEntity> =
        this.mTripLiveData

    override fun observeResult(): BaseLiveData<TripEntity> =
        this.mTripLiveData

    override fun updateTrip(tripId: String) =
        this.updateTripUseCase.executeEveryTime(
            2,
            PostTripObserver(this.mUpdateLiveData),
            tripId
        )

    override fun observableTrackTrip(): BaseLiveData<TripEntity> = this.mUpdateLiveData

    override fun onCleared() {
        this.postTripUseCase.dispose()
        this.updateTripUseCase.dispose()
        super.onCleared()
    }

}