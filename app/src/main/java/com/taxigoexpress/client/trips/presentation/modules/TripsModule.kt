package com.taxigoexpress.client.trips.presentation.modules

import com.taxigoexpress.core.presentation.scopes.FragmentScope
import com.taxigoexpress.client.trips.data.repositoryImplementations.TripsRepositoryImpl
import com.taxigoexpress.client.trips.data.services.MapsServices
import com.taxigoexpress.client.trips.data.services.TripServices
import com.taxigoexpress.client.trips.domain.entities.requestEntities.TripEntity
import com.taxigoexpress.client.trips.domain.entities.responseEntities.PriceResponse
import com.taxigoexpress.client.trips.domain.repositoryAbstractions.TripsRepository
import com.taxigoexpress.client.trips.presentation.viewmodel.abstraction.TripsHistoryViewModel
import com.taxigoexpress.client.trips.presentation.viewmodel.abstraction.MainTripViewModel
import com.taxigoexpress.client.trips.presentation.viewmodel.abstraction.PostTripViewModel
import com.taxigoexpress.client.trips.presentation.viewmodel.implementation.TripsHistoryViewModelImpl
import com.taxigoexpress.client.trips.presentation.viewmodel.implementation.MainTripViewModelImpl
import com.taxigoexpress.client.trips.presentation.viewmodel.implementation.PostTripViewModelImpl
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Named

@Module
class TripsModule {

    @Provides
    @FragmentScope
    fun providesTripServices(@Named("main_retrofit") retrofit: Retrofit): TripServices =
        retrofit.create(TripServices::class.java)

    @Provides
    @FragmentScope
    fun providesMapsServices(@Named("google_maps_retrofit") retrofit: Retrofit): MapsServices =
        retrofit.create(MapsServices::class.java)

    @Provides
    @FragmentScope
    fun providesTripsRepository(tripsRepositoryImpl: TripsRepositoryImpl): TripsRepository =
        tripsRepositoryImpl

    @Provides
    @FragmentScope
    fun providesTripsViewModel(tripsViewModelImpl: MainTripViewModelImpl): MainTripViewModel<List<PriceResponse>> =
        tripsViewModelImpl

    @Provides
    @FragmentScope
    fun providesTripsHistoryViewModel(tripsHistoryViewModel: TripsHistoryViewModelImpl): TripsHistoryViewModel<List<TripEntity>> =
        tripsHistoryViewModel

    @Provides
    @FragmentScope
    fun providesPostTripViewModel(postTripViewModelImpl: PostTripViewModelImpl): PostTripViewModel<TripEntity> =
        postTripViewModelImpl

}