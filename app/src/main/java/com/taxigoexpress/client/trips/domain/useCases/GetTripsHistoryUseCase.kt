package com.taxigoexpress.client.trips.domain.useCases

import com.taxigoexpress.client.trips.domain.entities.requestEntities.TripEntity
import com.taxigoexpress.core.domain.executors.PostExecutionThread
import com.taxigoexpress.core.domain.executors.ThreadExecutor
import com.taxigoexpress.core.domain.useCases.UseCase
import com.taxigoexpress.client.trips.domain.repositoryAbstractions.TripsRepository
import io.reactivex.Observable
import io.reactivex.ObservableSource
import javax.inject.Inject

class GetTripsHistoryUseCase
@Inject constructor(
    private val tripsRepository: TripsRepository,
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread
) :
    UseCase<List<TripEntity>, String>(threadExecutor, postExecutionThread) {

    override fun createObservable(params: String): Observable<List<TripEntity>> =
        this.tripsRepository.getTripsHistory()
            .flatMap {
                Observable.just(
                    this.getPassengerTrips(it, params)
                )
            }


    private fun getPassengerTrips(
        tripsHistory: List<TripEntity>,
        params: String
    ): List<TripEntity> {
        return tripsHistory.filter { x ->
            x._id_pas == params.toInt()
        }
    }


}