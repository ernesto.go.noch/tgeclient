package com.taxigoexpress.client.trips.domain.useCases

import com.taxigoexpress.core.domain.executors.PostExecutionThread
import com.taxigoexpress.core.domain.executors.ThreadExecutor
import com.taxigoexpress.core.domain.useCases.UseCase
import com.taxigoexpress.client.trips.domain.repositoryAbstractions.TripsRepository
import io.reactivex.Observable
import javax.inject.Inject

class GetTripTypesUseCase
@Inject constructor(
    private val tripsRepository: TripsRepository,
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread
) : UseCase<Unit, Unit>(threadExecutor, postExecutionThread) {

    override fun createObservable(params: Unit): Observable<Unit> =
        this.tripsRepository.getTripTypes()


}