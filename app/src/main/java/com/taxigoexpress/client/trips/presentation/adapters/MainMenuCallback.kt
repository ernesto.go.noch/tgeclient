package com.taxigoexpress.client.trips.presentation.adapters

interface MainMenuCallback {
    fun onGetMenuOption(option: MainMenuEnum)

    enum class MainMenuEnum {
        TripRequest,
        Coupons,
        TripsHistory,
        Payment,
        CloseSession
    }
}
