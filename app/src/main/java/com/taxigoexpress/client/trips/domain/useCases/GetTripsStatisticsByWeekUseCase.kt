package com.taxigoexpress.client.trips.domain.useCases

import com.taxigoexpress.client.trips.domain.entities.requestEntities.TripStatisticsRequest
import com.taxigoexpress.client.trips.domain.entities.responseEntities.TripStatisticsResponse
import com.taxigoexpress.client.trips.domain.repositoryAbstractions.TripsRepository
import com.taxigoexpress.client.trips.presentation.utils.TripsUtils
import com.taxigoexpress.core.domain.executors.PostExecutionThread
import com.taxigoexpress.core.domain.executors.ThreadExecutor
import com.taxigoexpress.core.domain.useCases.UseCase
import io.reactivex.Observable
import javax.inject.Inject

class GetTripsStatisticsByWeekUseCase
@Inject constructor(
    private val tripsRepository: TripsRepository,
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread
) :
    UseCase<TripStatisticsResponse, Unit>(threadExecutor, postExecutionThread) {

    override fun createObservable(params: Unit): Observable<TripStatisticsResponse> =
        this.tripsRepository.getTripsStatistics(
            TripStatisticsRequest(
                this.getStartDate(),
                this.getEndDate()
            )
        )

    private fun getEndDate(): String =
        TripsUtils.getStartDate(TripsUtils.Period.WEEK)


    private fun getStartDate(): String =
        TripsUtils.formattedDate(TripsUtils.getEndWeekDay())

}

