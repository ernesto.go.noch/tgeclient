package com.taxigoexpress.client

import android.os.Bundle
import android.view.Gravity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.ia.mchaveza.kotlin_library.gone
import com.ia.mchaveza.kotlin_library.visible
import com.taxigoexpress.client.profile.presentation.fragments.ProfileFragment
import com.taxigoexpress.core.payment.presentation.fragments.PaymentMethodFragment
import com.taxigoexpress.client.trips.presentation.adapters.MainMenuAdapter
import com.taxigoexpress.client.trips.presentation.adapters.MainMenuCallback
import com.taxigoexpress.client.trips.presentation.fragments.MainTripViewFragment
import com.taxigoexpress.client.trips.presentation.fragments.TripsHistoryFragment
import com.taxigoexpress.core.presentation.activities.BaseActivity
import com.taxigoexpress.core.presentation.utils.Constants
import com.taxigoexpress.core.presentation.utils.clickEvent
import com.taxigoexpress.register.presentation.activities.RegisterActivity
import com.taxigoexpress.signin.presentation.activities.SignInActivity
import kotlinx.android.synthetic.main.activity_home_layout.*
import kotlinx.android.synthetic.main.content_home_toolbar.*
import kotlinx.android.synthetic.main.navigation_view.*
import org.jetbrains.anko.intentFor

class HomeActivity : BaseActivity() {

    override fun getLayoutResId(): Int = R.layout.activity_home_layout

    override fun initView(savedInstanceState: Bundle?) {
        this.checkCredentials()
        this.launchView()
        this.configureView()
        this.configureToolbar()

    }

    private fun checkCredentials() {
        if (this.keyStoreManager.retrieveDataWithSafeMode(Constants.USER).isNullOrEmpty()) {
            this.startActivity(intentFor<RegisterActivity>())
        } else {
            this.startActivity(intentFor<SignInActivity>())
        }
    }

    private fun launchView() {
//        Places.initialize(this, this.getString(R.string.api_key_google_maps))
//        val placesClient = Places.createClient(this)
        this.replaceFragment(MainTripViewFragment(), R.id.home_container, false)

//        this.startActivity(intentFor<SignInActivity>())
    }

    private fun configureToolbar() {
        setSupportActionBar(homeToolbar)

        this.ivToolbarBack.clickEvent().observe(this, Observer {
            if (this.supportFragmentManager.backStackEntryCount > 0) {
                this.supportFragmentManager.popBackStack()
            } else {
                this.finish()
            }
        })
    }

    private fun configureView() {

        this.ivToolbarMenu.clickEvent().observe(this, Observer {
            this.drawerLayout.openDrawer(Gravity.LEFT)
        })

        this.tvProfile.clickEvent().observe(this, Observer {
            this.replaceFragment(
                ProfileFragment(),
                R.id.home_container,
                true
            )
        })

        val options = this.resources.getStringArray(R.array.arr_menu)
        val adapter = MainMenuAdapter(options.toList(), this)
        this.rvMenuOptions.adapter = adapter
        adapter.setMenuListener(object : MainMenuCallback {
            override fun onGetMenuOption(option: MainMenuCallback.MainMenuEnum) {
                val fragment = when (option) {
                    MainMenuCallback.MainMenuEnum.TripRequest -> MainTripViewFragment()
                    MainMenuCallback.MainMenuEnum.Coupons -> PaymentMethodFragment()
                    MainMenuCallback.MainMenuEnum.TripsHistory -> TripsHistoryFragment.newInstance()
                    MainMenuCallback.MainMenuEnum.Payment -> PaymentMethodFragment.newInstance(false)
                    else -> PaymentMethodFragment.newInstance(false)
                }
                if (fragment !is MainTripViewFragment) {
                    this@HomeActivity.replaceFragment(
                        fragment,
                        R.id.home_container,
                        false
                    )
                }
                this@HomeActivity.drawerLayout.closeDrawer(Gravity.LEFT)
            }
        })

        this.rvMenuOptions.layoutManager = LinearLayoutManager(this)
    }

    fun openDrawer() {
        this.drawerLayout.openDrawer(Gravity.LEFT)
    }

    fun hideToolbar() {
        this.homeAppbar.gone()
    }

    fun showToolbar() {
        this.homeAppbar.visible()
    }

    fun changeToolbarTitle(title: String) {
        this.tvToolbarTitle.text = title
    }
}
