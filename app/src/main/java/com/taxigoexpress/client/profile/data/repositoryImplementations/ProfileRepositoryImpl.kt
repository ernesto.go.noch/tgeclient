package com.taxigoexpress.client.profile.data.repositoryImplementations

import com.taxigoexpress.client.profile.data.datasourceImplementations.ProfileDataSourceImpl
import com.taxigoexpress.client.profile.domain.repositoryAbstractions.ProfileRepository
import com.taxigoexpress.core.domain.entities.UserEntity
import io.reactivex.Observable
import javax.inject.Inject

class ProfileRepositoryImpl
@Inject constructor(private val profileDataSourceImpl: ProfileDataSourceImpl) : ProfileRepository {

    override fun getProfileInformation(profileId: String): Observable<UserEntity> =
        this.profileDataSourceImpl.getProfileInformation(profileId)
}