package com.taxigoexpress.client.profile.domain.datasourceAbstractions

import com.taxigoexpress.core.domain.entities.UserEntity
import io.reactivex.Observable

interface ProfileDataSource {
    fun getProfileInformation(profileId: String): Observable<UserEntity>
}
