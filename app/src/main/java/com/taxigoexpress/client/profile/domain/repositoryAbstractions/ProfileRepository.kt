package com.taxigoexpress.client.profile.domain.repositoryAbstractions

import com.taxigoexpress.core.domain.entities.UserEntity
import io.reactivex.Observable

interface ProfileRepository {
    fun getProfileInformation(profileId: String): Observable<UserEntity>
}
