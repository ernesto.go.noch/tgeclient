package com.taxigoexpress.client.profile.presentation.components

import com.taxigoexpress.core.presentation.components.ApplicationComponent
import com.taxigoexpress.core.presentation.scopes.FragmentScope
import com.taxigoexpress.client.profile.presentation.fragments.ProfileFragment
import com.taxigoexpress.client.profile.presentation.modules.ProfileModule
import dagger.Component

@FragmentScope
@Component(
    dependencies = [ApplicationComponent::class],
    modules = [ProfileModule::class]
)
interface ProfileComponent {
    fun inject(profileFragment: ProfileFragment)
}