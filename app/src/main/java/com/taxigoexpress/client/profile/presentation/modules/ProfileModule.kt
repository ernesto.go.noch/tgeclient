package com.taxigoexpress.client.profile.presentation.modules

import com.taxigoexpress.core.presentation.scopes.FragmentScope
import com.taxigoexpress.client.profile.data.repositoryImplementations.ProfileRepositoryImpl
import com.taxigoexpress.client.profile.data.services.ProfileServices
import com.taxigoexpress.client.profile.domain.repositoryAbstractions.ProfileRepository
import com.taxigoexpress.client.profile.presentation.viewmodels.abstractions.ProfileViewModel
import com.taxigoexpress.client.profile.presentation.viewmodels.implementations.ProfileViewModelImpl
import com.taxigoexpress.core.domain.entities.UserEntity
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Named

@Module
class ProfileModule {

    @Provides
    @FragmentScope
    fun providesProfileServices(@Named("main_retrofit") retrofit: Retrofit): ProfileServices =
        retrofit.create(ProfileServices::class.java)

    @Provides
    @FragmentScope
    fun providesProfileRepository(profileRepositoryImpl: ProfileRepositoryImpl): ProfileRepository =
        profileRepositoryImpl

    @Provides
    @FragmentScope
    fun providesProfileViewModel(profileViewModelImpl: ProfileViewModelImpl): ProfileViewModel<UserEntity> =
        profileViewModelImpl
}