package com.taxigoexpress.client.profile.data.datasourceImplementations

import com.taxigoexpress.client.profile.data.services.ProfileServices
import com.taxigoexpress.client.profile.domain.datasourceAbstractions.ProfileDataSource
import com.taxigoexpress.core.domain.entities.UserEntity
import com.taxigoexpress.core.presentation.managers.ResponseManager
import io.reactivex.Observable
import javax.inject.Inject

class ProfileDataSourceImpl
@Inject constructor(private val service: ProfileServices) : ProfileDataSource {

    override fun getProfileInformation(profileId: String): Observable<UserEntity> =
        this.service.getProfile(profileId).flatMap {
            if (it.isSuccessful) {
                Observable.just(it.body())
            } else {
                Observable.error(ResponseManager.processWrongBody(it))
            }
        }
}
