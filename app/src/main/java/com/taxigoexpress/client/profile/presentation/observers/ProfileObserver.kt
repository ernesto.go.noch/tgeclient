package com.taxigoexpress.client.profile.presentation.observers

import android.view.View
import com.taxigoexpress.client.R
import com.taxigoexpress.core.domain.entities.UserEntity
import com.taxigoexpress.core.presentation.observers.DefaultObserverLiveData
import com.taxigoexpress.core.presentation.view.BaseLiveData
import java.io.IOException

class ProfileObserver(private val mProfileLiveData: BaseLiveData<UserEntity>) :
    DefaultObserverLiveData<UserEntity>(mProfileLiveData) {

    override fun onStart() {
        this.mProfileLiveData.loadingObserver.value = View.VISIBLE
        super.onStart()
    }

    override fun onComplete() {
        this.mProfileLiveData.loadingObserver.value = View.GONE
    }

    override fun onNext(result: UserEntity) {
        this.mProfileLiveData.customObserver.value = result
    }

    override fun onError(error: Throwable) {
        this.mProfileLiveData.loadingObserver.value = View.GONE
        when (error) {
            is IOException -> this.mProfileLiveData.showErrorObserver.value =
                R.string.no_internet_connection
            else -> this.mProfileLiveData.showExceptionObserver.value = error.localizedMessage
        }
    }

}
