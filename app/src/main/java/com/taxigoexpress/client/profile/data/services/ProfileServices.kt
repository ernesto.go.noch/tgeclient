package com.taxigoexpress.client.profile.data.services

import com.taxigoexpress.core.BuildConfig
import com.taxigoexpress.core.domain.entities.UserEntity
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface ProfileServices {

    @GET(BuildConfig.GET_PROFILE)
    fun getProfile(
        @Path("id") profileId: String
    ): Observable<Response<UserEntity>>
}
