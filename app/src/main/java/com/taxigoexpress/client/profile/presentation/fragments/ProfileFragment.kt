package com.taxigoexpress.client.profile.presentation.fragments

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.taxigoexpress.client.HomeActivity
import com.taxigoexpress.client.R
import com.taxigoexpress.client.profile.presentation.components.DaggerProfileComponent
import com.taxigoexpress.client.profile.presentation.components.ProfileComponent
import com.taxigoexpress.client.profile.presentation.modules.ProfileModule
import com.taxigoexpress.client.profile.presentation.viewmodels.abstractions.ProfileViewModel
import com.taxigoexpress.core.domain.entities.UserEntity
import com.taxigoexpress.core.getApplicationComponent
import com.taxigoexpress.core.presentation.fragments.BaseFragment
import com.taxigoexpress.core.presentation.fragments.LoaderFragment
import com.taxigoexpress.core.presentation.utils.Constants
import kotlinx.android.synthetic.main.fragment_profile_layout.*
import javax.inject.Inject

/**
 * A simple [Fragment] subclass.
 *
 */
class ProfileFragment : BaseFragment() {

    private val loaderDialog by lazy { LoaderFragment.newInstance(getString(com.taxigoexpress.register.R.string.loading)) }

    @Inject
    lateinit var profileViewModel: ProfileViewModel<UserEntity>

    private val profileComponent: ProfileComponent by lazy {
        DaggerProfileComponent.builder()
            .applicationComponent(activity?.getApplicationComponent())
            .profileModule(ProfileModule())
            .build()
    }

    override fun getResourceLayout(): Int = R.layout.fragment_profile_layout

    override fun initView(view: View, savedInstanceState: Bundle?) {
        this.profileComponent.inject(this)
        keyStoreManager?.retrieveDataWithSafeMode(
            Constants.PASS_ID
        )?.let {
            this.profileViewModel.getProfileInformation(it)
        }
        this.configureToolbar()

        this.observeResults()
    }

    private fun configureToolbar() {
        this.showToolbar()
            this.activity?.let {
                (it as HomeActivity).changeToolbarTitle(
                    this.getString(R.string.profile_title)
                )
            }
    }

    private fun loadProfileInformation(profile: UserEntity) {
        this.et_profile_name.setText(profile.name)
        this.et_profile_last_name.setText(profile.lastName)
        this.et_profile_mail.setText(profile.email)
        this.et_profile_phone.setText(profile.phoneNumber)
    }

    private fun observeResults() {
        with(this.profileViewModel.observeProfileInformation()) {
            this.customObserver.observe(this@ProfileFragment, Observer { profile ->
                profile?.let {
                    this@ProfileFragment.loadProfileInformation(it)
                }
            })

            this.loadingObserver.observe(this@ProfileFragment, Observer {
                if (it == View.VISIBLE) {
                    this@ProfileFragment.loaderDialog.show(
                        childFragmentManager,
                        ProfileFragment::class.java.name
                    )
                } else {
                    this@ProfileFragment.loaderDialog.dismiss()
                }
            })

            this.showExceptionObserver.observe(this@ProfileFragment, Observer {
                this@ProfileFragment.showAlert(it ?: "")
            })

            this.showErrorObserver.observe(this@ProfileFragment, Observer {
                this@ProfileFragment.showAlertWithResource(it ?: 0)
            })
        }
    }


}
