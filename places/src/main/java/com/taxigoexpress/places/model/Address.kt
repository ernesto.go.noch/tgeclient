package com.taxigoexpress.places.model

/**
 */
data class Address(
    val longName: String,
    val shortName: String,
    val type: ArrayList<String>
)