package com.taxigoexpress.places.adapter

import com.taxigoexpress.places.PlaceAPI
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageView
import android.widget.TextView
import com.ia.mchaveza.kotlin_library.gone
import com.ia.mchaveza.kotlin_library.visible
import com.taxigoexpress.places.R
import com.taxigoexpress.places.model.Place

/**
 */
class PlacesAutoCompleteAdapter(
    mContext: Context,
    val placesApi: PlaceAPI,
    private val addDrawableLeft: Boolean
) :
    ArrayAdapter<Place>(mContext, R.layout.autocomplete_list_item), Filterable {

    var resultList: ArrayList<Place>? = ArrayList()

    override fun getCount(): Int {
        return when {
            resultList.isNullOrEmpty() -> 0
            else -> resultList?.size!!
        }
    }

    override fun getItem(position: Int): Place? {
        return when {
            resultList.isNullOrEmpty() -> null
            else -> resultList!![position]
        }
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var view = convertView
        val viewHolder: ViewHolder
        if (view == null) {
            viewHolder = ViewHolder()
            val inflater = LayoutInflater.from(context)
            view = inflater.inflate(R.layout.autocomplete_list_item, parent, false)
            viewHolder.description = view.findViewById(R.id.autocompleteText) as TextView
            viewHolder.footerImageView = view.findViewById(R.id.footerImageView) as ImageView
            viewHolder.ivAddMarker = view.findViewById(R.id.ivAddPlaceMarker) as ImageView
            view.tag = viewHolder
        } else {
            viewHolder = view.tag as ViewHolder
        }
        val place = resultList!![position]
        bindView(viewHolder, place, position)
        return view!!
    }

    private fun bindView(viewHolder: ViewHolder, place: Place, position: Int) {
        if (!resultList.isNullOrEmpty()) {
            if (position != resultList!!.size - 1) {
                if (addDrawableLeft) {
                    viewHolder.ivAddMarker?.visible()
                } else {
                    viewHolder.ivAddMarker?.gone()
                }
                viewHolder.description?.text = place.description
                viewHolder.footerImageView?.visibility = View.GONE
                viewHolder.description?.visibility = View.VISIBLE
            } else {
                viewHolder.footerImageView?.visibility = View.VISIBLE
                viewHolder.description?.visibility = View.GONE
            }
        }
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                if (results != null && results.count > 0) {
                    notifyDataSetChanged()
                } else {
                    notifyDataSetInvalidated()
                }
            }

            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val filterResults = FilterResults()
                if (constraint != null) {
                    resultList = placesApi.autocomplete(constraint.toString())
                    resultList?.add(Place("-1", "footer"))
                    filterResults.values = resultList
                    filterResults.count = resultList!!.size
                }
                return filterResults
            }
        }
    }

    internal class ViewHolder {
        var ivAddMarker: ImageView? = null
        var description: TextView? = null
        var footerImageView: ImageView? = null
    }
}
