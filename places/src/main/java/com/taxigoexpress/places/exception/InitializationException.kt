package com.taxigoexpress.places.exception

/**
 */
class InitializationException(message: String?) : Exception(message)
