package com.taxigoexpress.core.payment.domain.exceptions

class PaymentCardException
@JvmOverloads constructor(
    val validationType: Type,
    message: String? = "Register ${validationType.name}",
    cause: Throwable? = null
) :
    Throwable(message, cause) {

    enum class Type {
        NO_VALID_CARD_OWNER,
        NO_VALID_CARD_NUMBER,
        NO_VALID_EXPIRATION_DATE,
        NO_VALID_CVV
    }
}