package com.taxigoexpress.core.payment.domain.datasourceAbstractions

import com.taxigoexpress.core.payment.domain.entities.requestEntities.PaymentCardRequest
import com.taxigoexpress.core.payment.domain.entities.responseEntities.PaymentCardResponse
import io.reactivex.Observable

interface PaymentDataSource {

    fun addPaymentCard(cardRequest: PaymentCardRequest): Observable<PaymentCardResponse>


}
