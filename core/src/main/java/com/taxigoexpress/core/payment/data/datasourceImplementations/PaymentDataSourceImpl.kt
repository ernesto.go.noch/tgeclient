package com.taxigoexpress.core.payment.data.datasourceImplementations

import com.taxigoexpress.core.payment.data.services.PaymentServices
import com.taxigoexpress.core.payment.domain.datasourceAbstractions.PaymentDataSource
import com.taxigoexpress.core.payment.domain.entities.requestEntities.PaymentCardRequest
import com.taxigoexpress.core.payment.domain.entities.responseEntities.PaymentCardResponse
import io.reactivex.Observable
import javax.inject.Inject

class PaymentDataSourceImpl
@Inject constructor(private val services: PaymentServices) : PaymentDataSource {

    override fun addPaymentCard(cardRequest: PaymentCardRequest): Observable<PaymentCardResponse> =
        this.services.addPaymentCard(
            cardRequest.pasId,
            cardRequest
        ).flatMap {
            if (it.isSuccessful) {
                Observable.just(it.body())
            } else {
                Observable.error(Throwable(it.message()))
            }
        }
}
