package com.taxigoexpress.core.payment.presentation.viewmodels.implementations

import com.taxigoexpress.core.payment.domain.entities.CardEntity
import com.taxigoexpress.core.payment.domain.entities.responseEntities.PaymentCardResponse
import com.taxigoexpress.core.payment.domain.useCases.AddPaymentCardUseCase
import com.taxigoexpress.core.payment.presentation.liveData.PaymentCardLiveData
import com.taxigoexpress.core.payment.presentation.observers.PaymentCardObserver
import com.taxigoexpress.core.payment.presentation.viewmodels.abstractions.PaymentCardViewModel
import com.taxigoexpress.core.presentation.view.BaseLiveData
import com.taxigoexpress.core.presentation.viewmodel.BaseViewModelLiveData
import javax.inject.Inject

class PaymentCardViewModelImpl
@Inject constructor(
    private val addPaymentCardUseCase: AddPaymentCardUseCase
) : BaseViewModelLiveData<PaymentCardResponse>(), PaymentCardViewModel<PaymentCardResponse> {

    private val mPaymentCardLiveData by lazy { PaymentCardLiveData(this.getCustomLiveData()) }

    override fun addPaymentCard(cardEntity: CardEntity) {
        this.addPaymentCardUseCase.execute(
            PaymentCardObserver(mPaymentCardLiveData),
            cardEntity
        )
    }

    override fun observeAddPaymentCard(): PaymentCardLiveData =
        this.mPaymentCardLiveData

    override fun observeResult(): BaseLiveData<PaymentCardResponse> =
        this.mPaymentCardLiveData.paymentCardObserver

    override fun onCleared() {
        this.addPaymentCardUseCase.dispose()
        super.onCleared()
    }
}