package com.taxigoexpress.core.payment.domain.entities

data class CardEntity(
    val id:String?,
    val cardNumber: String,
    val expirationDate: String,
    val cardCVV: String,
    val cardOwner: String
) {

}
