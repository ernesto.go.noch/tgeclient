package com.taxigoexpress.core.payment.presentation.components

import com.taxigoexpress.core.payment.presentation.fragments.AddPaymentCardFragment
import com.taxigoexpress.core.payment.presentation.modules.PaymentCardModule
import com.taxigoexpress.core.presentation.components.ApplicationComponent
import com.taxigoexpress.core.presentation.scopes.FragmentScope
import dagger.Component

@FragmentScope
@Component(
    dependencies = [ApplicationComponent::class],
    modules = [PaymentCardModule::class]
)
interface PaymentCardComponent {
    fun inject(addPaymentCardFragment: AddPaymentCardFragment)
}