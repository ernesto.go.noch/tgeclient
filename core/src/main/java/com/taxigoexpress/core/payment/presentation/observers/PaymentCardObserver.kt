package com.taxigoexpress.core.payment.presentation.observers

import android.view.View
import com.taxigoexpress.core.R
import com.taxigoexpress.core.payment.domain.entities.responseEntities.PaymentCardResponse
import com.taxigoexpress.core.payment.domain.exceptions.PaymentCardException
import com.taxigoexpress.core.payment.presentation.liveData.PaymentCardLiveData
import com.taxigoexpress.core.presentation.observers.DefaultObserverLiveData
import java.io.IOException

class PaymentCardObserver(private val mPaymentCardLiveData: PaymentCardLiveData) :
    DefaultObserverLiveData<PaymentCardResponse>(mPaymentCardLiveData.paymentCardObserver) {

    override fun onStart() {
        this.mPaymentCardLiveData.paymentCardObserver.loadingObserver.value = View.VISIBLE
        super.onStart()
    }

    override fun onComplete() {
        this.mPaymentCardLiveData.paymentCardObserver.loadingObserver.value = View.GONE
    }

    override fun onNext(result: PaymentCardResponse) {
        this.mPaymentCardLiveData.paymentCardObserver.customObserver.value = result
    }

    override fun onError(error: Throwable) {
        this.mPaymentCardLiveData.paymentCardObserver.loadingObserver.value = View.GONE
        when (error) {
            is IOException -> this.mPaymentCardLiveData.paymentCardObserver.showErrorObserver.value =
                R.string.no_internet_connection
            is PaymentCardException -> useCase(error)
            else -> this.mPaymentCardLiveData.paymentCardObserver.showExceptionObserver.value =
                error.localizedMessage
        }
    }

    private fun useCase(error: PaymentCardException) {
        when (error.validationType) {
            PaymentCardException.Type.NO_VALID_CARD_OWNER -> this.mPaymentCardLiveData.ownerNameErrorObserver.value =
                R.string.no_valid_name
            PaymentCardException.Type.NO_VALID_CARD_NUMBER -> this.mPaymentCardLiveData.cardNumberErrorObserver.value =
                R.string.no_valid_card_number
            PaymentCardException.Type.NO_VALID_CVV -> this.mPaymentCardLiveData.cvvErrorObserver.value =
                R.string.no_valid_cvv
            PaymentCardException.Type.NO_VALID_EXPIRATION_DATE -> this.mPaymentCardLiveData.expirationDateErrorObserver.value =
                R.string.no_valid_expiration_date
        }
    }

}
