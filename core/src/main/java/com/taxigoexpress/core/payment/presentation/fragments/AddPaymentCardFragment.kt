package com.taxigoexpress.core.payment.presentation.fragments

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import com.taxigoexpress.core.R
import com.taxigoexpress.core.getApplicationComponent
import com.taxigoexpress.core.payment.domain.entities.CardEntity
import com.taxigoexpress.core.payment.domain.entities.responseEntities.PaymentCardResponse
import com.taxigoexpress.core.payment.presentation.components.DaggerPaymentCardComponent
import com.taxigoexpress.core.payment.presentation.components.PaymentCardComponent
import com.taxigoexpress.core.payment.presentation.modules.PaymentCardModule
import com.taxigoexpress.core.payment.presentation.viewmodels.abstractions.PaymentCardViewModel
import com.taxigoexpress.core.presentation.fragments.BaseDialogFragment
import com.taxigoexpress.core.presentation.fragments.LoaderFragment
import com.taxigoexpress.core.presentation.utils.Constants
import com.taxigoexpress.core.presentation.utils.Constants.CARD_SCAN_REQUEST_CODE
import com.taxigoexpress.core.presentation.utils.clickEvent
import io.card.payment.CardIOActivity
import io.card.payment.CreditCard
import kotlinx.android.synthetic.main.fragment_add_payment_card_layout.*
import javax.inject.Inject


/**
 * A simple [DialogFragment] subclass.
 */
class AddPaymentCardFragment : BaseDialogFragment() {

    @Inject
    lateinit var paymentCardViewModel: PaymentCardViewModel<PaymentCardResponse>

    private val loaderDialog by lazy { LoaderFragment.newInstance(getString(R.string.loading)) }

    override fun getStyle(): Int? = R.style.FullScreenDialog

    override fun getLayout(): Int = R.layout.fragment_add_payment_card_layout

    override fun initView(view: View, savedInstanceState: Bundle?) {
        this.paymentCardComponent.inject(this)
        this.configureView()
        this.observeResults()
    }

    private val paymentCardComponent: PaymentCardComponent by lazy {
        DaggerPaymentCardComponent.builder()
            .applicationComponent(activity?.getApplicationComponent())
            .paymentCardModule(PaymentCardModule())
            .build()
    }

    private fun configureView() {
        this.isCancelable = false
        this.etExpirationDate.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            @SuppressLint("SetTextI18n")
            override fun onTextChanged(p0: CharSequence?, start: Int, removed: Int, added: Int) {
                if (start == 1 && start + added == 2 && p0?.contains('/') == false) {
                    etExpirationDate.setText("$p0/")
                } else if (start == 3 && start - removed == 2 && p0?.contains('/') == true) {
                    etExpirationDate.setText(p0.toString().replace("/", ""))
                }
            }
        })

        this.cvCaptureCard.clickEvent().observe(this, Observer {
            this.launchCardScanner()
        })

        this.ivCancelAddPaymentCard.clickEvent().observe(this, Observer {
            this.dismiss()
        })

        this.btnSavePaymentCard.clickEvent().observe(this, Observer {
            this.paymentCardViewModel.addPaymentCard(
                CardEntity(
                    this.keyStoreManager?.retrieveDataWithSafeMode(Constants.PASS_ID),
                    this.etCardnumber.text.toString(),
                    this.etExpirationDate.text.toString(),
                    this.etCVV2.text.toString(),
                    this.etCardNameOwner.text.toString()
                )
            )
        })
    }

    private fun launchCardScanner() {
        val scanIntent = Intent(activity, CardIOActivity::class.java)
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_POSTAL_CODE, false)
        scanIntent.putExtra(CardIOActivity.EXTRA_SUPPRESS_MANUAL_ENTRY, true)
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_EXPIRY, true)
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_CVV, true)
        this.startActivityForResult(scanIntent, CARD_SCAN_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == CARD_SCAN_REQUEST_CODE) {
            data?.let {
                if (it.hasExtra(CardIOActivity.EXTRA_SCAN_RESULT)) {
                    val scanResult: CreditCard =
                        data.getParcelableExtra(CardIOActivity.EXTRA_SCAN_RESULT)
                    this.setInformationCard(scanResult)
                }
            }
        }
    }

    private fun setInformationCard(scanResult: CreditCard) {
        val cc = this.generateCreditCardEntity(scanResult)
        this.etCardnumber.setText(cc.cardNumber)
        this.etCardNameOwner.setText(cc.cardOwner)
        this.etCVV2.setText(cc.cardCVV)
        this.etExpirationDate.setText(cc.expirationDate)
    }

    private fun generateCreditCardEntity(scanResult: CreditCard): CardEntity {
        return CardEntity(
            this.keyStoreManager?.retrieveDataWithSafeMode(Constants.PASS_ID),
            scanResult.cardNumber,
            scanResult.expiryMonth.toString() +
                    scanResult.expiryYear.toString(),
            scanResult.cvv,
            scanResult.cardholderName
        )
    }

    private fun observeResults() {

        with(this.paymentCardViewModel.observeAddPaymentCard()) {

            this.paymentCardObserver.customObserver.observe(this@AddPaymentCardFragment, Observer {
            })

            this.paymentCardObserver.loadingObserver.observe(this@AddPaymentCardFragment, Observer {
                if (it == View.VISIBLE) {
                    this@AddPaymentCardFragment.loaderDialog.show(
                        childFragmentManager,
                        AddPaymentCardFragment::class.java.name
                    )
                } else {
                    this@AddPaymentCardFragment.loaderDialog.dismiss()
                }
            })

            this.ownerNameErrorObserver.observe(this@AddPaymentCardFragment, Observer {
                this@AddPaymentCardFragment.showAlertWithResource(it ?: 0)
            })

            this.cardNumberErrorObserver.observe(this@AddPaymentCardFragment, Observer {
                this@AddPaymentCardFragment.showAlertWithResource(it ?: 0)
            })

            this.cvvErrorObserver.observe(this@AddPaymentCardFragment, Observer {
                this@AddPaymentCardFragment.showAlertWithResource(it ?: 0)
            })

            this.expirationDateErrorObserver.observe(this@AddPaymentCardFragment, Observer {
                this@AddPaymentCardFragment.showAlertWithResource(it ?: 0)
            })


            this.paymentCardObserver.showErrorObserver.observe(
                this@AddPaymentCardFragment,
                Observer {
                    this@AddPaymentCardFragment.showAlertWithResource(it ?: 0)
                })

            this.paymentCardObserver.showExceptionObserver.observe(
                this@AddPaymentCardFragment,
                Observer {
                    this@AddPaymentCardFragment.showAlert(it)
                })

        }

    }

}
