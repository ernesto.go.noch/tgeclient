package com.taxigoexpress.core.payment.data.repositoryImplementations

import com.taxigoexpress.core.payment.data.datasourceImplementations.PaymentDataSourceImpl
import com.taxigoexpress.core.payment.domain.entities.requestEntities.PaymentCardRequest
import com.taxigoexpress.core.payment.domain.entities.responseEntities.PaymentCardResponse
import com.taxigoexpress.core.payment.domain.repositoryAbstractions.PaymentRepository
import io.reactivex.Observable
import javax.inject.Inject

class PaymentRepositoryImpl
@Inject constructor(private val paymentDataSourceImpl: PaymentDataSourceImpl):
    PaymentRepository {

    override fun addPaymentCard(cardRequest: PaymentCardRequest):
            Observable<PaymentCardResponse> =
        this.paymentDataSourceImpl.addPaymentCard(cardRequest)



}