package com.taxigoexpress.core.payment.presentation.adapters

import com.taxigoexpress.core.payment.presentation.utils.PaymentUtils

interface PaymentMethodCallback {
    fun onPaymentMethodSelected(method: PaymentUtils.Method)
}
