package com.taxigoexpress.core.payment.presentation.utils

object PaymentUtils {

    enum class Method {
        Card,
        Cash
    }
}
