package com.taxigoexpress.core.payment.data.services

import com.taxigoexpress.core.payment.domain.entities.requestEntities.PaymentCardRequest
import com.taxigoexpress.core.payment.domain.entities.responseEntities.PaymentCardResponse
import com.taxigoexpress.core.BuildConfig
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Path

interface PaymentServices {

    @POST(BuildConfig.ADD_PAYMENT_CARD)
    fun addPaymentCard(
        @Path("pas_id") pasId: String,
        @Body cardRequest: PaymentCardRequest
    ): Observable<Response<PaymentCardResponse>>

}
