package com.taxigoexpress.core.payment.domain.entities.requestEntities

import com.google.gson.annotations.SerializedName

data class PaymentCardRequest(

    val _id: String? = null,

    @field:SerializedName("num_tar")
    val _cardNumber: String? = null,

    @field:SerializedName("nombre")
    val _cardOwner: String? = null,

    @field:SerializedName("cvv2")
    val _cardCVV: String? = null,

    @field:SerializedName("expir_M")
    val _cardExpirationMonth: String? = null,

    @field:SerializedName("expir_Y")
    val _cardExpirationYear: String? = null,

    @field:SerializedName("last_4")
    val _cardLastDigits: String? = null
) {

    val pasId: String
        get() = _id ?: ""

    val cardNumber: String
        get() = _cardNumber ?: ""

    val cardOwner: String
        get() = _cardOwner ?: ""

    val cardCVV: String
        get() = _cardCVV ?: ""

    val cardExpirationMonth: String
        get() = _cardExpirationMonth ?: ""

    val cardExpirationYear: String
        get() = _cardExpirationYear ?: ""

    val cardLastDigits: String
        get() = _cardLastDigits ?: ""

}
