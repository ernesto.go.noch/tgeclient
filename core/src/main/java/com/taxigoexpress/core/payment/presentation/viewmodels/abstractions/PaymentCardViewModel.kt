package com.taxigoexpress.core.payment.presentation.viewmodels.abstractions

import com.taxigoexpress.core.payment.domain.entities.CardEntity
import com.taxigoexpress.core.payment.domain.entities.requestEntities.PaymentCardRequest
import com.taxigoexpress.core.payment.presentation.liveData.PaymentCardLiveData
import com.taxigoexpress.core.presentation.viewmodel.PresenterLiveData

interface PaymentCardViewModel<T> : PresenterLiveData<T> {
    fun addPaymentCard(cardEntity: CardEntity)
    fun observeAddPaymentCard(): PaymentCardLiveData
}