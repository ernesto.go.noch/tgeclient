package com.taxigoexpress.core.payment.domain.repositoryAbstractions

import com.taxigoexpress.core.payment.domain.entities.requestEntities.PaymentCardRequest
import com.taxigoexpress.core.payment.domain.entities.responseEntities.PaymentCardResponse
import io.reactivex.Observable

interface PaymentRepository {
    fun addPaymentCard(cardRequest: PaymentCardRequest): Observable<PaymentCardResponse>
}
