package com.taxigoexpress.core.payment.presentation.modules

import com.taxigoexpress.core.payment.data.repositoryImplementations.PaymentRepositoryImpl
import com.taxigoexpress.core.payment.data.services.PaymentServices
import com.taxigoexpress.core.payment.domain.entities.responseEntities.PaymentCardResponse
import com.taxigoexpress.core.payment.domain.repositoryAbstractions.PaymentRepository
import com.taxigoexpress.core.payment.presentation.viewmodels.abstractions.PaymentCardViewModel
import com.taxigoexpress.core.payment.presentation.viewmodels.implementations.PaymentCardViewModelImpl
import com.taxigoexpress.core.presentation.scopes.FragmentScope
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Named

@Module
class PaymentCardModule {

    @Provides
    @FragmentScope
    fun providesPaymentServices(@Named("main_retrofit") retrofit: Retrofit): PaymentServices =
        retrofit.create(PaymentServices::class.java)

    @Provides
    @FragmentScope
    fun providesPaymentRepository(paymentRepositoryImpl: PaymentRepositoryImpl): PaymentRepository =
        paymentRepositoryImpl

    @Provides
    @FragmentScope
    fun providesPaymentCardViewmodel(paymentCardViewModelImpl: PaymentCardViewModelImpl): PaymentCardViewModel<PaymentCardResponse> =
        paymentCardViewModelImpl
}