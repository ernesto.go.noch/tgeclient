package com.taxigoexpress.core.payment.presentation.adapters

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ia.mchaveza.kotlin_library.inflate
import com.taxigoexpress.core.R
import com.taxigoexpress.core.payment.presentation.utils.PaymentUtils
import com.taxigoexpress.core.presentation.utils.clickEventObservable
import kotlinx.android.synthetic.main.item_payment_method_layout.view.*

class PaymentMethodAdapter(private val context: Context) :
    RecyclerView.Adapter<PaymentMethodAdapter.ViewHolder>() {

    private var listener: PaymentMethodCallback? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(parent.inflate(R.layout.item_payment_method_layout))

    override fun getItemCount(): Int = 2

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.updateItem(position, context)
        holder.itemView.clickEventObservable().subscribe {
            when (position) {
                0 -> this.listener?.onPaymentMethodSelected(PaymentUtils.Method.Card)
                1 -> this.listener?.onPaymentMethodSelected(PaymentUtils.Method.Card)
                else -> this.listener?.onPaymentMethodSelected(PaymentUtils.Method.Cash)
            }
        }
    }

    fun setPaymentMethodListener(listener: PaymentMethodCallback) {
        this.listener = listener
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun updateItem(position: Int, context: Context) {
            if (position == 0) {
                itemView.ivPaymentMethod.setImageDrawable(
                    context.getDrawable(R.drawable.payment_method)
                )
                itemView.tvPaymentMethodTitle.text = context.getString(R.string.payment_method_card)
                itemView.tvPaymentMethodSubtitle.text =
                    context.getString(R.string.payment_method_card_subtitle)
            } else {
                itemView.ivPaymentMethod.setImageDrawable(
                    context.getDrawable(R.mipmap.cash_payment_method)
                )
                itemView.tvPaymentMethodTitle.text = context.getString(R.string.payment_method_cash)
                itemView.tvPaymentMethodSubtitle.text =
                    context.getString(R.string.payment_method_cash_subtitle)
            }
        }
    }
}