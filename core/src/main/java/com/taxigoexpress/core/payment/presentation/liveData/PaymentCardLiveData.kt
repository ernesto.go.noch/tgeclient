package com.taxigoexpress.core.payment.presentation.liveData

import androidx.lifecycle.MutableLiveData
import com.taxigoexpress.core.payment.domain.entities.responseEntities.PaymentCardResponse
import com.taxigoexpress.core.presentation.view.BaseLiveData

data class PaymentCardLiveData(
    val paymentCardObserver: BaseLiveData<PaymentCardResponse>,
    val ownerNameErrorObserver: MutableLiveData<Int> = MutableLiveData(),
    val cardNumberErrorObserver: MutableLiveData<Int> = MutableLiveData(),
    val expirationDateErrorObserver: MutableLiveData<Int> = MutableLiveData(),
    val cvvErrorObserver: MutableLiveData<Int> = MutableLiveData()
) {

}
