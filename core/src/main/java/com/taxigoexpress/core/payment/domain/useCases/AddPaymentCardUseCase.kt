package com.taxigoexpress.core.payment.domain.useCases

import com.taxigoexpress.core.payment.domain.entities.CardEntity
import com.taxigoexpress.core.payment.domain.entities.requestEntities.PaymentCardRequest
import com.taxigoexpress.core.payment.domain.entities.responseEntities.PaymentCardResponse
import com.taxigoexpress.core.payment.domain.repositoryAbstractions.PaymentRepository
import com.taxigoexpress.core.payment.domain.exceptions.PaymentCardException
import com.taxigoexpress.core.domain.executors.PostExecutionThread
import com.taxigoexpress.core.domain.executors.ThreadExecutor
import com.taxigoexpress.core.domain.useCases.UseCase
import com.taxigoexpress.core.presentation.utils.isValidCardNumber
import io.reactivex.Observable
import javax.inject.Inject

class AddPaymentCardUseCase
@Inject constructor(
    private val paymentRepository: PaymentRepository,
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread
) : UseCase<PaymentCardResponse, CardEntity>
    (threadExecutor, postExecutionThread) {

    override fun createObservable(params: CardEntity): Observable<PaymentCardResponse> = when {
        params.cardOwner.length <= 2 -> Observable.error(
            PaymentCardException(
                PaymentCardException.Type.NO_VALID_CARD_OWNER
            )
        )
        !params.cardNumber.isValidCardNumber() -> Observable.error(
            PaymentCardException(
                PaymentCardException.Type.NO_VALID_CARD_NUMBER
            )
        )
        !this.isValidCardCVV(params.cardCVV, params.cardNumber) -> Observable.error(
            PaymentCardException(
                PaymentCardException.Type.NO_VALID_CVV
            )
        )
        params.expirationDate.length < 5 -> Observable.error(
            PaymentCardException(
                PaymentCardException.Type.NO_VALID_EXPIRATION_DATE
            )
        )
        else -> this.paymentRepository.addPaymentCard(this.generateRequest(params))
    }

    private fun isValidCardCVV(cardCVV: String, cardNumber: String): Boolean {
        return if (cardNumber.first().toString() == "3") {
            cardCVV.length == 4
        } else {
            cardCVV.length >= 3
        }
    }

    private fun generateRequest(params: CardEntity): PaymentCardRequest {
        return PaymentCardRequest(
            params.id,
            params.cardNumber,
            params.cardOwner,
            params.cardCVV,
            this.getExpirationMonth(params.expirationDate),
            this.getExpirationYear(params.expirationDate),
            this.getCardLastDigits(params.cardNumber)
        )
    }

    private fun getCardLastDigits(cardNumber: String): String {
        cardNumber.trim()
        return cardNumber.substring(cardNumber.length - 4)
    }

    private fun getExpirationYear(expirationDate: String): String {
        val arrExpiration = expirationDate.split("/")
        return "20" + arrExpiration.last()
    }

    private fun getExpirationMonth(expirationDate: String): String {
        val arrExpiration = expirationDate.split("/")
        return arrExpiration.first()
    }

}
