package com.taxigoexpress.core.payment.presentation.fragments

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.taxigoexpress.core.R
import com.taxigoexpress.core.presentation.fragments.BaseFragment
import com.taxigoexpress.core.payment.presentation.adapters.PaymentMethodAdapter
import com.taxigoexpress.core.payment.presentation.adapters.PaymentMethodCallback
import com.taxigoexpress.core.payment.presentation.utils.PaymentUtils
import kotlinx.android.synthetic.main.fragment_payment_method_layout.*

/**
 * A simple [Fragment] subclass.
 *
 */
class PaymentMethodFragment : BaseFragment() {

    private var isRegistering: Boolean = false

    override fun getResourceLayout(): Int = R.layout.fragment_payment_method_layout

    override fun initView(view: View, savedInstanceState: Bundle?) {
        this.loadArguments()
        this.loadAdapter()
    }

    private fun loadArguments() {
        this.arguments?.let {
            this.isRegistering = it.getBoolean(IS_REGISTERING)
        }
    }

    private fun loadAdapter() {
        this.context?.let {
            val adapter = PaymentMethodAdapter(it)
            this.rvPaymentMethod.adapter = adapter
            this.rvPaymentMethod.layoutManager = LinearLayoutManager(it)
            adapter.setPaymentMethodListener(object : PaymentMethodCallback {
                override fun onPaymentMethodSelected(method: PaymentUtils.Method) {
                    when (method) {
                        PaymentUtils.Method.Card -> this@PaymentMethodFragment.showAddPaymentCard()
                        else -> if (isRegistering) this@PaymentMethodFragment.activity?.finish() else
                            this@PaymentMethodFragment.activity?.supportFragmentManager?.popBackStack()
                    }

                }
            })
        }
    }

    private fun showAddPaymentCard() {
        AddPaymentCardFragment().show(
            this@PaymentMethodFragment.childFragmentManager,
            AddPaymentCardFragment::class.java.name
        )
    }

    companion object {
        private const val IS_REGISTERING = "is.registering"

        fun newInstance(isRegistring: Boolean) =
            PaymentMethodFragment().apply {
                this.arguments.apply {
                    isRegistring to IS_REGISTERING
                }
            }
    }


}
