package com.taxigoexpress.core.presentation.managers

import android.content.Intent
import androidx.fragment.app.Fragment
import com.facebook.accountkit.Account
import com.facebook.accountkit.AccountKit
import com.facebook.accountkit.AccountKitCallback
import com.facebook.accountkit.AccountKitError
import com.facebook.accountkit.ui.AccountKitActivity
import com.facebook.accountkit.ui.AccountKitConfiguration
import com.facebook.accountkit.ui.LoginType


class AccountKitManager {

    interface Callback {
        fun onAuthenticationSuccess(account: Account)
        fun onAuthenticationFailed(error: AccountKitError)
    }

    companion object {

        const val APP_REQUEST_CODE = 1101

        fun getCurrentAccount(context: Fragment, callback: Callback) {
            val accessToken = AccountKit.getCurrentAccessToken()
            accessToken?.let {
                AccountKit.getCurrentAccount(object : AccountKitCallback<Account> {
                    override fun onSuccess(account: Account?) {
                        account?.let {
                            callback.onAuthenticationSuccess(it)
                        }
                    }

                    override fun onError(error: AccountKitError?) {
                        error?.let {
                            callback.onAuthenticationFailed(error)
                        }
                    }
                })
            } ?: run {
                launchAccountKitActivity(
                    context
                )
            }
        }

        private fun launchAccountKitActivity(context: Fragment) {
            val intent = Intent(context.activity, AccountKitActivity::class.java)
            val configurationBuilder = AccountKitConfiguration.AccountKitConfigurationBuilder(
                LoginType.PHONE, AccountKitActivity.ResponseType.TOKEN
            )
            /*If you want default country code*/
            configurationBuilder.setDefaultCountryCode("MX")
            intent.putExtra(AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION, configurationBuilder.build())
            context.startActivityForResult(intent,
                APP_REQUEST_CODE
            )
        }
    }

}