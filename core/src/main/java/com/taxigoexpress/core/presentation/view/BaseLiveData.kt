package com.taxigoexpress.core.presentation.view

import androidx.lifecycle.MutableLiveData


data class BaseLiveData<Result>(
    val customObserver: MutableLiveData<Result>,
    val loadingObserver: MutableLiveData<Int>,
    val showErrorObserver: MutableLiveData<Int>,
    val showExceptionObserver: MutableLiveData<String>
)