package com.taxigoexpress.core.presentation.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.taxigoexpress.core.presentation.view.BaseLiveData

abstract class BaseViewModelLiveData<Result> : ViewModel() {

    fun getCustomLiveData(): BaseLiveData<Result> =
        BaseLiveData(
            MutableLiveData(),
            MutableLiveData(),
            MutableLiveData(),
            MutableLiveData()
        )

    fun <Q> buildLiveData(observer: MutableLiveData<Q>): BaseLiveData<Q> =
        BaseLiveData(observer, MutableLiveData(), MutableLiveData(), MutableLiveData())

}