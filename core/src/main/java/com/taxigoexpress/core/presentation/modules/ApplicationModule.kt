package com.taxigoexpress.core.presentation.modules

import android.accounts.AccountManager
import android.app.Application
import android.content.Context
import android.content.res.Resources
import com.taxigoexpress.core.domain.executors.JobExecutor
import com.taxigoexpress.core.domain.executors.PostExecutionThread
import com.taxigoexpress.core.domain.executors.ThreadExecutor
import com.taxigoexpress.core.domain.executors.UIThread
import com.taxigoexpress.core.presentation.authentication.services.KeyStoreManager
import com.ia.mchaveza.kotlin_library.SharedPreferencesManager
import com.ia.mchaveza.kotlin_library.TrackingManager
import dagger.Module
import dagger.Provides
import org.jetbrains.anko.accountManager
import javax.inject.Singleton

@Module
class ApplicationModule(private val application: Application) {

    @Provides
    @Singleton
    fun providesApplicationContext(): Application = this.application

    @Provides
    @Singleton
    fun providesContext(): Context = application.applicationContext

    @Provides
    @Singleton
    fun providesResources(): Resources = application.resources

    @Provides
    @Singleton
    fun provideSharedPreferences(application: Application): SharedPreferencesManager =
        SharedPreferencesManager(application.applicationContext)

    @Provides
    @Singleton
    fun provideKeyStoreManager(
        application: Application,
        preferencesManager: SharedPreferencesManager
    ): KeyStoreManager =
        KeyStoreManager(application.applicationContext, preferencesManager)

    @Provides
    @Singleton
    fun trackingManager(context: Context): TrackingManager =
        TrackingManager(context)

    @Provides
    @Singleton
    fun provideThreadExecutor(jobExecutor: JobExecutor): ThreadExecutor = jobExecutor

    @Provides
    @Singleton
    fun providePostExecutionThread(uiThread: UIThread): PostExecutionThread = uiThread

    @Provides
    @Singleton
    fun providesAccountManager(context: Context): AccountManager = context.accountManager

}