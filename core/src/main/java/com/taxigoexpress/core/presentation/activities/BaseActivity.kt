package com.taxigoexpress.core.presentation.activities

import android.os.Bundle
import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import androidx.appcompat.app.AppCompatActivity
import android.view.MenuItem
import com.ia.mchaveza.kotlin_library.SharedPreferencesManager
import com.ia.mchaveza.kotlin_library.performReplacingTransaction
import com.taxigoexpress.core.R
import com.taxigoexpress.core.presentation.authentication.services.KeyStoreManager


abstract class BaseActivity : AppCompatActivity() {

    val preferencesManager by lazy { SharedPreferencesManager(this) }

    val keyStoreManager by lazy { KeyStoreManager(this, preferencesManager) }

    private val transitions by lazy { Transitions(R.anim.left_in, R.anim.left_out, R.anim.rigth_in, R.anim.rigth_out) }

    abstract fun getLayoutResId(): Int

    abstract fun initView(savedInstanceState: Bundle?)

    override fun onCreate(savedInstanceState: Bundle?) {
        this.overridePendingTransition(transitions.onCreateEnterAnimation, transitions.onCreateExitAnimation)
        super.onCreate(savedInstanceState)
        if (getLayoutResId() != 0) {
            setContentView(getLayoutResId())
        }
        initView(savedInstanceState)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        this.overridePendingTransition(
            transitions.onBackPressedEnterAnimation,
            transitions.onBackPressedExitAnimation
        )
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                this.onBackPressed()
                true
            }
            else -> false
        }
    }

    class Transitions(
        val onCreateEnterAnimation: Int,
        val onCreateExitAnimation: Int,
        val onBackPressedEnterAnimation: Int,
        val onBackPressedExitAnimation: Int
    )

    open fun replaceFragment(fragment: Fragment, @IdRes container: Int, addToBackStack: Boolean) {
        supportFragmentManager.performReplacingTransaction(
            container,
            fragment,
            this.transitions.onCreateEnterAnimation,
            this.transitions.onCreateExitAnimation,
            this.transitions.onBackPressedEnterAnimation,
            this.transitions.onBackPressedExitAnimation,
            if (addToBackStack) fragment::class.java.name else null,
            false
        )
    }

    open fun replaceFragmentNoAnimation(fragment: Fragment, @IdRes container: Int, addToBackStack: Boolean) {
        supportFragmentManager.performReplacingTransaction(
            container,
            fragment,
            backStackTag = if (addToBackStack) fragment::class.java.name else null,
            allowStateLoss = false
        )
    }
}