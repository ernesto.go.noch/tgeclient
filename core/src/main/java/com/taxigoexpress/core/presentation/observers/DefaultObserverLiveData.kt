package com.taxigoexpress.core.presentation.observers

import com.taxigoexpress.core.presentation.view.BaseLiveData
import io.reactivex.observers.DisposableObserver

abstract class DefaultObserverLiveData<Result>(val observer: BaseLiveData<Result>) :
    DisposableObserver<Result>()