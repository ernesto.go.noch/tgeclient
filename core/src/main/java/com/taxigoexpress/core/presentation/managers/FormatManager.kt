package com.taxigoexpress.core.presentation.managers


object FormatManager {

    const val DATE_FORMAT = "dd MMM yyyy"
    private const val PERIOD_FORMAT = "dd 'de' MMMM 'de' yyyy"
    private const val MONTH_FORMAT = "MMM"
    private const val DAY_FORMAT = "dd"
    private const val TIME_FORMAT = "HH:mm 'h'"
    private const val PERIOD_FORMAT_NAME_DAY = "EEEE',' dd MMM yyyy"
    private const val DAY_NAME_MONTH_AND_YEAR_FORMAT = "EEEE',' MMMM dd 'de' yyyy"
    private const val FLIGHT_STATUS_DATE_FORMAT = "yyyy-MM-dd"
    private const val YEAR_MONTH_DAY_FORMAT = "yyyy-MM-dd"



}