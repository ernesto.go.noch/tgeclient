package com.taxigoexpress.core.presentation.managers

import com.taxigoexpress.core.domain.entities.BaseResponse
import com.taxigoexpress.core.domain.exceptions.TGException
import retrofit2.Response
import java.io.IOException

object ResponseManager {

    /**
     * Obtains a failure response out of a
     * 200 OK when it exists
     * @param response
     */
    fun processWrongBody(response: Response<*>): Throwable {
        return try {
            val body = response.body() as BaseResponse
            TGException(body.message, body.status.toString())
        } catch (ioException: IOException) {
            TGException("Error parsing response", ioException)
        }
    }

}