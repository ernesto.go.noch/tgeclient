package com.taxigoexpress.core.domain.entities

import com.google.gson.annotations.SerializedName
import java.io.Serializable

open class BaseResponse(

    @SerializedName(value = "status", alternate = ["ResponseCode", "BaseCode"])
    private var _status: Int? = null,

    @SerializedName("message")
    private var _message: String? = null

) : Serializable {
    val status: Int
        get() = _status ?: 0

    val message: String
        get() = _message ?: ""

    var error: BaseResponse
        get() = this
        set(response) {
            _message = response.message
            _status = response.status
        }
}
